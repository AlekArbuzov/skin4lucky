$(document).ready(function(){
    $('.change-image').on('click', function(e){
        e.preventDefault();
        var container = $(this).parent();
        container.find('#image-for-box[type="file"]').trigger('click');        
    });
    $('#image-for-box[type="file"]').on('change', function($val){
        var input = $(this);
        var formInput = $('.form').find('input[name="image-box"]');
        uploadItem(input, function(filename, fileurl){
            formInput.val(filename);
            $('#box-image').attr('src', fileurl);
        });
    });
    function uploadItem(input, ifSuccess) {
        var container = input.parents('[class *= profile-]');
        var errorSpan = container.find('span.error');
        var formData = new FormData();
        formData.append(input.attr('name'), input[0].files[0]);
        input.val('');
        $.ajax({
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: "json",
            success: function(response) {
                if (response.error) {
                    
                } else {
                    ifSuccess(response.filename, response.url);
                }
            },
        });
    }
})
