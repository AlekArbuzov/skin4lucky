$(function () {
    var moneyBlock = $('#moneyAmount');


    $('#btnSaveTradeUrl').on('click', function(e){
        e.preventDefault();
        var link = $('#tradeLink');
        var linkValue = link.val();
        if(linkValue.length <= 10){
            link.removeClass('success').addClass('fail');
            link.val('');
        }else{
            $.ajax({
                data:{tradeLink:linkValue},
                dataType:'json',
                type:'post',
                success:function(json){
                    if (json.status=='OK'){
                        link.removeClass('fail').addClass('success');
                    }else{
                        link.removeClass('success').addClass('fail');
                    }
                },
                error:function(data){
                    link.removeClass('success').addClass('fail');
                    console.log(data);
                }
            });
        }
    });

    $(document).on('click','.btn-send, .btn-sell',function(e){
        e.preventDefault();
        var btn = $(this);
        var action = $(this).attr('action');
        var drop = $(this).attr('drop-id');
        var pageProfile = $(this).hasClass('page-profile');
        $.ajax({
            url:base_url + 'actionDropItem',
            type:'post',
            dataType:'json',
            data:{action:action,drop:drop,profile:pageProfile},
            beforeSend:function(){
                $(btn).parent('.action-drop').find('a').addClass('disabled');
            },
            success:function(json){
                $(btn).parent('.action-drop').empty().html(json.html);
                $(moneyBlock).text(json.money);
            },
            error:function(data){
                console.log(data);
            }
        });
    });



    //Load More function

    $('#loadMore').on('click', function(){
        var page = Number($(this).attr('page'));
        var maxPage = Number($(this).attr('maxPage'));
        var self = this;
        $.ajax({
            data: {page:page},
            type: 'POST',
            dataType: "json",
            success: function(response) {
                if(response.html){
                    $('#content_results').append(response.html);
                }
                if(maxPage > page){
                    $(self).attr('page', page + 1);
                }else{
                    $(self).parent().detach();
                }
            },
            error:function(){

            },
        })
    });

    $('.btn-login').on('click', function(e){
        e.preventDefault();
        $('#uLogin span').trigger('click');
    });

    $('#exampleModal .use-promocode').click(function(e) {
        e.preventDefault();
        $('#exampleModal').modal('hide');
        $('#promocode_popup').modal('show');
    });

    $('#promocode_popup').on('show.bs.modal', function() {
        $(this).removeClass('success');
        $(this).find('.form').removeClass('hidden');
        $(this).find('.success-message').addClass('hidden');
    });

    $('#promocode_popup').on('hidden.bs.modal', function() {
        if ($(this).hasClass('success'))
            location.reload();
    });

    $('#promocode_popup').on("submit", "form", function(e){
        e.preventDefault();

        var form = $(this);
        var post = {};
        $.ajax({
            type: 'POST',
            url: form.attr('action'),
            data: form.serializeArray(),
            success: function(html) {
                if (html == 'ok') {
                    $('#promocode_popup').addClass('success');
                    $('#promocode_popup').find('.form').addClass('hidden');
                    $('#promocode_popup').find('.success-message').removeClass('hidden');
                } else {
                    form.replaceWith(html);
                }
            }
        });
    });

    $('body').on('click', '.btn-buy', function(){
        var itemBlock = $(this).parent('.case-open');
        var boxId = $(this).attr('box-id');
        var animateBlock = $('<div id="animate-drops" class="clearfix"><div class="items-container"></div></div>');
        $.ajax({
            url:base_url + 'openCase/' + boxId,
            type:'post',
            dataType:'json',
            beforeSend:function(){
                $(itemBlock).empty().append(animateBlock);
                twistItems();
            },
            success:function(json){
                drops.getDrops();
                if(json.result){
                    $(document).trigger('customEvent', json);
                    $(moneyBlock).text(json.money);
                }
            },
            error:function(data){
                console.log(data);
                alert('Ошибка запроса. Обновите страницу');
            }
        });
    });

    function isMobile() {
        try{ document.createEvent("TouchEvent"); return true; }
        catch(e){ return false; }
    }

    function playSound(sound) {
        if(!isMobile()){
            var audio = new Audio();
            audio.src = base_url + 'assets/sounds/' + sound; // Указываем путь к звуку "клика"
            audio.play();
        }
    }
    function twistItems(){
        playSound('start.mp3');
        var boxBlock = $('#animate-drops>.items-container');
        $('.box-item-block').each(function(){
            boxBlock.append($(this).clone());
        });
        var stepLength = 5;
        var decline = false;
        var divider = 1;
        var left = 0;
        var itemWidth = $('#animate-drops .box-item-block').first().outerWidth(true);
        var rand = Math.random() * (itemWidth-20) + 20;
        var item = false;
        var mainBlock = $('#animate-drops');
        var centerMain = mainBlock.offset().left + (mainBlock.width()/2);

        var sliderBlock = $('.items-container');
        var offsetSliderBlock = sliderBlock.offset().left;
        var difference = centerMain - offsetSliderBlock - itemWidth;

        var itemId = false;
        var soundSwitcher = true;
        var start = Date.now();


        var responseHtml = '';

        $(document).on('customEvent', function(event, responseData){
            responseHtml = responseData.html;
            if(responseData.error){
                clearInterval(sliderItems);
                $('.case-open').empty().append(responseHtml)
            }
            itemId = responseData.itemId;
        });

        var sliderItems = setInterval(function() {
            if(decline && Date.now()-start > 8000){
                divider += 0.005;
            }
            left += (stepLength/divider);
            if(item){
                var offsetItemCenter = item.offset().left + rand ;
                if(divider > 4 && centerMain > offsetItemCenter){
                    playSound('end.mp3');
                    clearInterval(sliderItems);
                    setTimeout(function(){
                        $('.case-open').empty().append(responseHtml)
                    }, 1000);
                }
            }

            if(left > ( -difference - 2) && left < (-difference + 2)){
                if(soundSwitcher){
                    playSound('zvuk.mp3');
                    soundSwitcher = false;
                }

            }
            if(left >= itemWidth){
                left -= itemWidth;
                soundSwitcher = true;
                if(itemId){
                    item = $(document).find('.items-container [rel='+ itemId +']');
                }
                var first = $('#animate-drops .box-item-block').first();
                $('#animate-drops>.items-container').append(first);
                sliderBlock.css('left', '-' + left + 'px');

                if(item && item.attr('rel') == first.find('.box-item-image').attr('rel')){
                    decline = true;
                }
            }else{
                sliderBlock.css('left', '-' + left + 'px');
            }
        }, 5)
    }

});