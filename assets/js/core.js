$(function() { 
    $('input.date').datepicker({dateFormat:"dd.mm.yy",changeMonth: true,changeYear: true,yearRange:'1930:2020'});
    $('input.datetime').datetimepicker({dateFormat:"dd.mm.yy",changeMonth: true,changeYear: true}); 
    $("a.colorbox").colorbox({transition:"none",maxWidth:"100%",maxHeight:"100%"});
    $("div.info").delay(5000).slideUp();
    
    var date = new Date;
    $.array_element_id = date.getTime();
    $("a.array_element_add").on("click",function(e){
        e.preventDefault();
        var tpl = $(this).prev();
        if (!tpl.hasClass("array_element_template")) tpl = tpl.find(".array_element_template");
        var el = tpl.clone().show().removeClass("array_element_template")
        tpl.before(el);
        var key = $.array_element_id++;
        el.find("*").each(function(){
            var name = $(this).attr("name");
            if (name) {
                $(this).attr("name",$(this).attr("name").replace('{id}',key));
            }
        });
    });
    $('body').on("click","a.array_element_del",function(e){
        e.preventDefault();
        $(this).parents(".item:first").remove();
    });
    
    $(".browse_wrap a").on("click",function(){$.colorbox({href:$(this).attr("href")});return false;});
    $(".browse_file").each(function (){
        var input = $(this);
        var url = input.val();
        input.wrap("<table class='browse_wrap' style='border:none;border-collapse:collapse'><tr><td style='vertical-align:middle;padding:0;margin:0'>");
        var row = input.parent().parent();
        
        var classList = input.attr('class').split(/\s+/);
        var type = 'files';
        $.each(classList,function(c,cls){
            if (cls.indexOf('type-')==0) {
                type = cls.replace("type-","");
            }
        });
        
        if (input.hasClass("image")) {
            var td;
            row.prepend(
                td = $("<td>").css({
                    "vertical-align":"middle",
                    "text-align":"center",
                    width:100,
                    height:100,
                    border:"1px solid #ccc",
                    margin:0,padding:0
                })
            )
            input.parent().css({padding:"0 0 0 5px"})
            if (url) {
                td.append(
                    $("<a>")
                        .attr({href:base_url+"/upload/CMS/"+type+"/"+url})
                        .append(
                            $("<img>")
                                .css({display:"block",margin:"auto"})
                                .attr({src:base_url+"/upload/CMS/.thumbs/"+type+"/"+url})
                        )
                )
            }
        }
        row.append(
            $("<td>").css({width:1,"vertical-align":"middle"}).append(
                $("<button data-type='"+type+"' class='browse_file_button single' type='button'>"+browse_text+"</button>")
            )
        )
    });
    $(".browse_file_button").on("click",function(){
        var button = this;
        var input = $(button).parent().parent().find('input');
        var type = $(this).attr("data-type");
        var path = type+"/"+input.val();
        
        window.KCFinder = {
            callBack: function(url) {
                url = decodeURIComponent(url);
                window.KCFinder = null;
                url = url.replace(base_url+"/upload/CMS/"+type+"/","");
                input.val(url);
                
                if (input.hasClass("image")) {
                    input.parent().parent().find("td:first img").remove();
                    input.parent().parent().find("td:first").append(
                        $("<a>")
                            .attr({href:base_url+"/upload/CMS/"+type+"/"+url})
                            .append(
                                $("<img>")
                                    .css({display:"block",margin:"auto"})
                                    .attr({src:base_url+"/upload/CMS/.thumbs/"+type+"/"+url})
                            )
                    )
                }
            }
        };
        window.open(base_url+"/admin/files/browse.php?type="+type+"&lang="+locale_lang+"&path="+encodeURIComponent(path), 'kcfinder_textbox',
            'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
            'resizable=1, scrollbars=0, width=800, height=600'
        );
    });

    $("fieldset.foldable > span.legend").prepend($("<a class='fold_button' href='#'>"));
    var cookie = $.cookie("bingo_foldings");
    if (cookie) cookie = $.evalJSON(cookie);
    $("a.fold_button").each(function(index){
        var fset = $(this).parent().parent();
        if (fset.attr("id") && cookie && cookie[location.href] && cookie[location.href][fset.attr("id")]) {
            fset.addClass("folded");
        }
    });
    $("a.fold_button").click(function(e) {
        e.preventDefault();
        var fset = $(this).parent().parent();
        var folded = fset.hasClass('folded');
        if (folded) fset.removeClass('folded'); else fset.addClass('folded');
        if (fset.attr("id")) {
            var cookie = $.cookie("bingo_foldings");
            if (!cookie) cookie = {}; else cookie = $.evalJSON(cookie);
            if (!cookie[location.href]) cookie[location.href] = {};
            cookie[location.href][fset.attr("id")] = !folded;
            $.cookie("bingo_foldings",$.toJSON(cookie));
        }
    });

    $("fieldset.widgets.available .fieldset-content > fieldset").draggable({
        helper:"clone",
        connectToSortable: "fieldset.widgets:not(.available) .fieldset-content"
    });
    $("fieldset.widgets").find("input,select,textarea").each(function(){
        $(this).attr("originalName",$(this).attr("name"));
    });
    $("fieldset.widgets:not(.available) > .fieldset-content").each(function(){
        var place = $(this).parent().attr("id");
        var layout = $(this).parent().parent().parent().attr("id");
        var key = $.array_element_id++;
        $(this).find("input,select,textarea").each(function(){
            var name = $(this).attr("originalName");
            $(this).attr("name","widgets["+layout+"]["+place+"]["+key+"]["+name+"]");
        });
    });

    $("fieldset.widgets:not(.available) .fieldset-content").sortable({
        items:">fieldset",
        connectWith: 'fieldset.widgets:not(.available) .fieldset-content',
        receive: function (e,ui) {
            var place = $(this).parent().attr("id");
            var layout = $(this).parent().parent().parent().attr("id");
            var key = $.array_element_id++;
            newItem.find("input,select,textarea").each(function(){
                var name = $(this).attr("originalName");
                $(this).attr("name","widgets["+layout+"]["+place+"]["+key+"]["+name+"]");
            });
        },
        beforeStop: function (e,ui) {
            newItem = ui.item;
        }
    });

    $("table.menu_editor").each(function(){
        $(this).find("tr").each(function(){
            if ($(this).find("td").length && $(this).hasClass("item"))
                $(this).prepend("<td class='handle' style='width:0;cursor:move;text-align:center;'>&nbsp;↕&nbsp;</td>");
            else if ($(this).next().hasClass("item"))
                $(this).prepend("<th></th>");
        })
    }).sortable({
        items:"tr.item",
        axis: "y",
        handle: ".handle",
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        }
    });
    
    $("input.tags").tagsInput({
        width: "auto",
        height: "12px"
    });
    
    $("input.post_tags").tagsInput({
        width: "auto",
        height: "12px",
        autocomplete_url: window.all_tags
    });

    $("div.tabs").each(function(){
        var tabs = $(this);
        var sel = 0;
        tabs.find("> input").each(function(){
            var val = $(this).val();
            tabs.find(">div").each(function(i){
                if ($(this).attr("data-value")==val) sel = i;
            });
        });
        
        var o = {
            active: sel,
            activate: function (e,ui) {
                var val = $(ui.newPanel || ui.panel).attr("data-value") || "";
                tabs.find("> input").val(val);
            }
        }
        o.selected = o.active;
        o.select = o.activate;
        tabs.tabs(o);
    });
    
    function urlLit (w,v) {
        var tr='a b v g d e ["zh","j"] z i y k l m n o p r s t u f h c ch sh ["shh","shch"] ~ y ~ e yu ya ~ ["jo","e"]'.split(' ');
        var ww=''; w=w.toLowerCase();
        v = v || 0;
        for(i=0; i<w.length; ++i) {
            cc=w.charCodeAt(i); 
            ch=(cc>=1072?tr[cc-1072]:w[i]);
            if(ch.length<3) 
                ww+=ch; 
            else 
                ww+=eval(ch)[v];
        }
    
        return(ww.replace(/[^a-zA-Z0-9\-]/g,'-').replace(/[-]{2,}/gim, '-').replace( /^\-+/g, '').replace( /\-+$/g, ''));
    }
    
    $("input.item-title").each(function(){
        var target = $("input.item-permalink");
        $(this).data("old-value",$(this).val());
        $(this).bind("keyup change click",function(){
            var old = $(this).data("old-value") || "";
            var cur = $(this).val();
            
            var url = target.val();
            var oldUrl = urlLit(old);
            var curUrl = urlLit(cur);
            
            if (!url || oldUrl == url) {
                target.val(curUrl);
            }
            $(this).data("old-value",cur);
        });
    });    
    
});
