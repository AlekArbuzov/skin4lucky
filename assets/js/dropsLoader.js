function DropsLoader() {
    var self = this;
    
    this.refreshFrequency= 10;
    this.lastId = 0;
    this.dropBlock = '.drops-items-block';
    this.dropCountBlock = '.cases-open-count';
    
    this.init = function() {
        $(function() {
            self.lastId = $('.one-drop-item:first').attr('id-drop');
            self.getDrops();        
            $(window).on('focus', function() {
                self.refreshFrequency = 5;
                self.getDrops();
            });
            $(window).on('blur', function() {
                self.refreshFrequency = 60;
            });
        });
    };
    
    this.getDrops = function () {
        clearTimeout(self.refreshTimeout);
        $.post( base_url+'getLastDrops', {lastId:self.lastId} , function (response) {
            $.each(response.drops, function(key, value){
                self.addDrop(value);
                self.lastId = key;
            });
            $(self.dropCountBlock).text(response.count);
        }, 'json');
        
        self.refreshTimeout = setTimeout(function() {
            self.getDrops();
        }, this.refreshFrequency * 1000);
    };
    
    this.addDrop = function(drop){
        $(self.dropBlock).prepend(drop);
        $(self.dropBlock).find('.one-drop-item:last').detach();
    }
    
    this.init();
}

var drops = new DropsLoader();
