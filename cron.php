<?php

ini_set('short_open_tag', 'On');
define('BINGO_PATH',__DIR__.'/../bingo');
require_once BINGO_PATH . "/loader.php";
require_once('vendor/autoload.php');

ini_set('display_errors',1);
error_reporting(E_ALL ^ E_DEPRECATED);
date_default_timezone_set('Europe/Kiev');

\Bingo\Configuration::$applicationMode = 'development';
\Bingo\Configuration::$locale = 'ru_RU';
\Bingo\Configuration::addModulePath(INDEX_DIR."/modules");
\Bingo\Configuration::addModules('Auth', 'FileManager', 'CMS', 'Lucky');


require __DIR__."/db.php";

\CMS\Configuration::$log_errors = true;


\Bingo\Bingo::getInstance()->setup(); 

$route = false;
if (php_sapi_name() === 'cli') {
    $route = @$argv[1];
}
if (!$route) exit;

try 
{
    $cron = new \Lucky\Controllers\Cron();
    $cron->run($route);
}
 catch (\Exception $e) {
    \Bingo\Action::run('global_error',array('EXCEPTION', $e->getMessage(), $e->getFile(), $e->getLine(), $e->getTrace()));
}
