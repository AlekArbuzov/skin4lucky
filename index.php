<?php

ini_set('short_open_tag', 'On');
define('BINGO_PATH',__DIR__.'/../bingo');
require_once BINGO_PATH . "/loader.php";
require_once('vendor/autoload.php');

ini_set('display_errors',1);
error_reporting(E_ALL ^ E_DEPRECATED);
date_default_timezone_set('Europe/Kiev');

\Bingo\Configuration::$applicationMode = 'development';
\Bingo\Configuration::$locale = 'ru_RU';
\Bingo\Configuration::addModulePath(INDEX_DIR."/modules");
\Bingo\Configuration::addModules('Auth', 'FileManager', 'CMS', 'Lucky');
bingo_domain_register('lucky',dirname(__FILE__)."/locale");
bingo_domain('lucky');
require __DIR__."/db.php";
\Bingo\Template::addIncludePath('', BINGO_PATH."/template", INDEX_URL."/../bingo/template");
\Bingo\Template::addIncludePath('', INDEX_DIR."/template", INDEX_URL."/template");

\CMS\Configuration::$log_errors = true;

\Bingo\Bingo::getInstance()->run();
