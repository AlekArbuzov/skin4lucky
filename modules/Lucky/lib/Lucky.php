<?php

class Lucky extends \Bingo\Module {
    function __construct()
    {
        parent::__construct();

        \Bingo\Config::loadFile('config', INDEX_DIR."/config.php");
        $this->addModelPath(__DIR__."/Lucky/Models");
        
        array_unshift(
            \Bingo\Template::$baseDirectories,
            array(
                'path'=>INDEX_DIR."/template/admin",
                'url'=>INDEX_URL."/template/admin",
                'prefix'=>'admin'
            )
        );
        
        \Bingo\Template::addIncludePath('admin',
            INDEX_DIR."/template/admin",
            INDEX_URL."/template/admin"
        );
        
        //Main controller action mapping
        $this->connect("/", ['controller'=>'Lucky\Controllers\Main', 'action'=>'index']);
        $this->connect("/test", ['controller'=>'Lucky\Controllers\Test', 'action'=>'index']);
        $this->connect("/:action", ['controller' => 'Lucky\Controllers\User'],
            ['action' => '(check|logout|profile|use-promocode)']);
        $this->connect("/:action/:id", ['controller'=>'Lucky\Controllers\Drops', 'id' => false],
            ['action'=>'(openCase|actionDropItem|getLastDrops)']
        );
        $this->connect("/:action/:id", ['controller'=>'Lucky\Controllers\Main', 'id' => false],
            ['action'=>'(box|faq|partner|gifts)']
        );
        $this->connect("/:action/:slug", ['controller'=>'Lucky\Controllers\Pages', 'slug' => false,
            'function'=>function($route){
                \Bingo\Routing::$controller = new $route['controller']();
                $reflection = new ReflectionMethod(\Bingo\Routing::$controller, $route['action']);
                $parametrs = $reflection->getParameters();
                $className = $parametrs[0]->getClass()->name;
                if(isset($route['slug']) && $className){
                    $page = $className::findOneBy(['slug'=>$route['slug']]);
                    if($page){
                        return \Bingo\Routing::$controller->$route['action']($page);
                    }else{
                        return \Bingo\Routing::$controller->$route['action']($route['slug']);
                    }
                }else{
                    return true;
                }
            }],
            ['action'=>'(page)']
        );
        $this->connect("/:action/:id", ['controller'=>'Lucky\Controllers\Payment', 'id' => false],
            ['action'=>'(purchase)']
        );

        $this->connect("/:action/:id", ['controller'=>'Lucky\Controllers\Cron', 'id' => false],
            ['action'=>'(sendDrops|makeAutoPurchase|makeTradePurchase)']
        );

        //Admin
        $this->connect("/admin", ['controller'=>'Lucky\Controllers\Admin\Main', 'action'=>'index']);
        $this->connect('/admin/:action/:id',
                       array('controller'=>'Lucky\Controllers\Admin\Main','id'=>false,'priority'=>11111),
                       ['action' => '(item-list|item-edit|box-list|category-list|box-edit|category-edit|box-delete|item-delete|refresh-list)']
        );
        $this->connect('/admin/:action/:id',
                       array('controller'=>'Lucky\Controllers\Admin\Bot','id'=>false,'priority'=>11111),
                       ['action' => '(bot|autotrade)']
        );
        $this->connect('/admin/partner-program/:action/:id',array('controller'=>'Lucky\Controllers\Admin\PartnerProgram','id'=>false),
            ['action' => '(referral-list|settings|partner-type-list|partner-type-edit|bonus-list)']
        );
        $this->connect('/admin/:action/:id',array('controller'=>'Lucky\Controllers\Admin\User','id'=>false),
            ['action' => '(user-edit|user-list|referral-list|sales-list|drops-list|partner-type-list|partner-type-edit|youtuber-account-links-edit)']
        );
        $this->connect('/admin/promocodes/:action',array('controller'=>'Lucky\Controllers\Admin\Promocodes'),
            ['action' => '(promolist|generate)']
        );
        $this->connect('/admin/:action/:id',array('controller'=>'Lucky\Controllers\Admin\Pages','id'=>false),
            ['action' => '(page-list|page-edit|page-delete)']
        );
        $this->connect('/admin/:action/:id',array('controller'=>'Lucky\Controllers\Admin\Articles','id'=>false),
            ['action' => '(faq-list|faq-type-list|faq-edit|faq-type-edit|faq-type-delete|faq-delete)']
        );
        $this->connect('/admin/developer/:action/:id',array('controller'=>'Lucky\Controllers\Admin\Developer','id'=>false));
        
        \Bingo\Action::add('admin_pre_header',
        function () {
            \Bingo\Action::add('admin_header', function(){
            ?>
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
            <link href="<?php echo url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
            <link href="<?php echo url('assets/css/admin.css')?>" rel="stylesheet">
            <link href="<?php echo url('assets/css/sb-admin.css')?>" rel="stylesheet">
            <link href="<?php echo url('assets/css/morris.css')?>" rel="stylesheet">
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <script src="<?php echo url('assets/js/admin.js')?>" type="text/javascript"></script>
            <?php
        });
            $myMenu = array('Главная'=>'/admin');
            \Admin::$menu = array_merge($myMenu, \Admin::$menu);
            \Admin::$menu['Пользователи']['Пользователи'] = 'admin/user-list';
            \Admin::$menu['Пользователи']['Оплата'] = 'admin/sales-list';
            \Admin::$menu['Пользователи']['Дроп'] = 'admin/drops-list';

            \Admin::$menu['Партнерская программа']['Список рефералов'] = 'admin/partner-program/referral-list';
            \Admin::$menu['Партнерская программа']['Типы'] = 'admin/partner-program/partner-type-list';
            \Admin::$menu['Партнерская программа']['Настройки'] = 'admin/partner-program/settings';
            \Admin::$menu['Партнерская программа']['Бонусы'] = 'admin/partner-program/bonus-list';

            \Admin::$menu['Пользователи']["Ловушки для youtube'ров"] = 'admin/youtuber-account-links-edit';

            \Admin::$menu['Статьи']['Страницы'] = 'admin/page-list';
            \Admin::$menu['Статьи']['Категории FAQ'] = 'admin/faq-type-list';
            \Admin::$menu['Статьи']['Список FAQ'] = 'admin/faq-list';
            \Admin::$menu['Кейсы']['Категории'] = 'admin/category-list';
            \Admin::$menu['Кейсы']['Сундуки'] = 'admin/box-list';
            \Admin::$menu['Кейсы']['Предметы'] = 'admin/item-list';
            \Admin::$menu['Промокоды'] = 'admin/promocodes/promolist';
            \Admin::$menu['Бот']['Статус'] = 'admin/bot';
            \Admin::$menu['Бот']['Закупки'] = 'admin/autotrade';
        });
    }
}
