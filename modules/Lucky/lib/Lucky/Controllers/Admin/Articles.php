<?php

namespace Lucky\Controllers\Admin;

class Articles extends Main
{
    public function faq_list(){
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'id';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';
        
        $query = \Lucky\Models\Articles::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);
        
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['id'] = _t('id');
        $this->data['fields']['title'] = _t('Заголовок');
        $this->data['fields']['article_type'] = _t('Раздел');
        $this->data['fields']['text'] = _t('Текст');
        $this->data['field_filters']['text'] = function($val, $obj) {
            return mb_substr(strip_tags($val),0,200).'...';
        };
        $this->data['item_actions']['admin/faq-edit'] = _t('Изменить');
        $this->data['item_actions']['admin/faq-delete'] = _t('Удалить');
        $this->data['sort_fields'] = ['id', 'name'];
        
        $this->data['page_actions']['admin/faq-edit'] = _t('Создать новую');
       
        $form = new \Bingo\Form;
        $this->data['form'] = $form->get();
        $this->data['title'] = _t("FAQ статьи"); 
        $this->view('cms/base-list');

    }
    
    public function faq_edit($id){
        $article = \Lucky\Models\Articles::findOrCreate($id);
        foreach(\Lucky\Models\ArticleType::findAll() as $article_type) $article_types[$article_type->name] = $article_type->id;
        $form = new \Bingo\Form;
        $form->fieldset(_t('Введите необходимые поля'));
        $form->text('title', _t('Заголовок'), 'required', $article->title);
        $form->select('article_type', _t('Тип статьи'), $article_types, '', (isset($article->article_type->id)) ? $article->article_type->id: "");
        $form->textarea('text', 'Текст', 'required', $article->text)->add_class("tinymce");
        $form->submit(_t('Сохранить'));
              
        if($form->validate()) {
            $form->fill($article);
            $article->article_type = \Lucky\Models\ArticleType::find($article->article_type);
            $article->save();
            set_flash('info',_t('Successfully saved'));
            redirect('admin/faq-list');
        }
        
        $this->data['form'] = $form->get();
        $this->data['title'] =  ($id) ? _t("Изменить статью") : _t("Создать статью");
        $this->view('cms/base-edit');
    }
    
    public function faq_delete($id) {
        $article = \Lucky\Models\Articles::find($id);
        $article->delete();
        redirect('admin/faq-list');
    }    
    
    public function faq_type_list(){        
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'id';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';
        
        $query = \Lucky\Models\ArticleType::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);
        
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['id'] = 'id';
        $this->data['fields']['name'] = _t('Название');
        $this->data['item_actions']['admin/faq-type-edit'] = _t('Изменить');
        $this->data['item_actions']['admin/faq-type-delete'] = _t('Удалить');
        $this->data['sort_fields'] = ['id', 'name'];
        
        $this->data['page_actions']['admin/faq-type-edit'] = _t('Создать новую');
       
        $form = new \Bingo\Form;
        $this->data['form'] = $form->get();
        $this->data['title'] = _t("Категории статей"); 
        $this->view('cms/base-list');
    }
    
    public function faq_type_edit($id){
        
        $article_type = \Lucky\Models\ArticleType::findOrCreate($id);
        
        $form = new \Bingo\Form;
        $form->fieldset(_t('Введите необходимые поля'));
        $form->text('name', _t('Название'), 'required', $article_type->name);
        $form->submit(_t('Сохранить'));
              
        if($form->validate()) {
            $form->fill($article_type);    
            $article_type->save();
            set_flash('info',_t('Successfully saved'));
            redirect('admin/faq-type-list');
        } 
        
        $this->data['form'] = $form->get();
        $this->data['title'] =  ($id) ? _t("Редактировать категорию статьи") : _t("Создание категории статьи"); 
        $this->view('cms/base-edit');
    }
    
    public function faq_type_delete($id) {
        $article_type = \Lucky\Models\ArticleType::find($id);
        $article_type->delete();
        redirect('admin/faq-type-list');
    }    
}