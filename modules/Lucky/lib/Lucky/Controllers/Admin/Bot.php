<?php

namespace Lucky\Controllers\Admin;

use Bingo\Config;
use CMS\Controllers\Admin\BasePrivate;
use Lucky\CsGoApi;
use Lucky\Models\AutomatedPurchase;

class Bot extends BasePrivate  {
    
    public function bot(){
        $configBots = Config::get('config', 'bots');
        $connectSteam = new \Lucky\Models\Bot($configBots); 
        $this->data['bots'] = $connectSteam->getBots();
        $this->data['title'] = _t("Статус бота"); 
        $this->view('cms/bot');
    }

    public function autotrade(){
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'id';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';

        $query = AutomatedPurchase::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);

        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);

        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['foreign_id'] = _t('Id закупки');
        $this->data['fields']['item'] = _t('Предмет');
        $this->data['fields']['price'] = _t('Цена закупки');
        $this->data['fields']['status'] = _t('Статус');
        $this->data['fields']['botName'] = _t('Имя бота');
        $this->data['fields']['date'] = _t('Дата');
        $this->data['field_filters']['status'] = function($val, $obj) {
            return $obj->getStatusLabel();
        };

        $this->data['sort_fields'] = ['foreign_id', 'status', 'price', 'item'];
        $this->data['title'] = _t('Список закупок CSGO');
        $this->view('cms/base-list');
    }
}