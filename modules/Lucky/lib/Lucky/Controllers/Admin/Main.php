<?php

namespace Lucky\Controllers\Admin;
use Lucky\SteamApi;

class Main extends \CMS\Controllers\Admin\BasePrivate {

    public function index() {
        
        $this->data['last_referral'] = \Lucky\Models\Referral::findBy([], 'id desc',0,8);
        
        $this->data['count_users'] = count(\Lucky\Models\User::findAll());
        $this->data['last_users'] = \Lucky\Models\User::findBy([], 'id desc',0,8);
        $this->data['last_transactions'] = \Lucky\Models\Transaction::findBy(['type'=>'BUY'], 'id desc',0,8);
        $allTransactions = \Lucky\Models\Transaction::findBy(['type'=>'BUY']);
        $sum = 0;
        foreach($allTransactions as $transaction){
            $sum += $transaction->price;
        }
        $allDrops = \Lucky\Models\Drops::findAll();
        $sumMoneyDrops = 0;
        foreach($allDrops as $drop){
            $sumMoneyDrops += $drop->box->price;
        }
        $this->data['sum_transactions'] = $sum;
        $this->data['sum_money_drops'] = $sumMoneyDrops;
        $this->data['title'] = _t('Административная панель');
        $this->view('cms/main');
    }
    
    public function item_list() {
        
        $_GET['sort_by'] = (isset($_GET['sort_by'])) ? $_GET['sort_by']: 'fake';
        $_GET['sort_order'] = (isset($_GET['sort_order']))? $_GET['sort_order'] : '';
        
        $query = \Lucky\Models\Item::findByQuery(['deleteAt'=>NULL], $_GET['sort_by']." ".$_GET['sort_order']);
        
        $pagination = new \Bingo\Pagination(30,$this->getPage(),false,false,$query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['id'] = _t('id');
        $this->data['fields']['name'] = _t('Название');
        $this->data['fields']['icon_url'] = _t('Изображение');
        $this->data['fields']['amount'] = _t('Кол-во');
        $this->data['fields']['fake'] = _t('Фиктивный');
        $this->data['fields']['openCount'] = _t('Кол-ов открытий');
        $this->data['fields']['chance'] = _t('Шанс выпадения');
        $this->data['fields']['box'] = _t('Сундук');
        $this->data['fields']['addToBoxAt'] = _t('Дата добавления в сундук');
        $this->data['field_filters']['icon_url'] = function($val) {
            return '<img style="max-width: 50px;" class="box-item-image" src="http://cdn.steamcommunity.com/economy/image/'.$val.'">';
        };
        $this->data['field_filters']['box'] = function($val) {
            if(isset($val)) return _t("Относиться к сундуку № ").$val->id." (".$val->name.")"; 
        };
        $this->data['field_filters']['chance'] = function($val) {
            if(isset($val)) return $val."%"; 
        };
        $this->data['sort_fields'] = ['id', 'name', 'box', 'fake', 'amount'];
        $this->data['page_actions']['admin/refresh-list'] = _t('Обновить список');
        $this->data['page_actions']['admin/item-edit'] = _t('Добавить предмет');
        $this->data['item_actions']['admin/item-edit'] = _t('изменить');
        $this->data['item_actions']['admin/item-delete'] = _t('удалить');
        $this->data['title'] = _t('Список предметов');
        $this->view('cms/base-list');
    }
    
    public function refresh_list() {
        /** Обнуление кол-ва предметов в базе */
        $qb = \Bingo::$em->createQueryBuilder();
        $itemsDb = $qb->update("\Lucky\Models\Item", 'i')
            ->set('i.fake', true)
            ->set('i.amount', 0)
            ->getQuery();
        $itemsDb->execute();

        $steamApi = new SteamApi();
        $configBots = \Bingo\Config::get('config', 'bots');
        $error = '';
        foreach ($configBots as $oneBot){
            $items = $steamApi->getInventory($oneBot['steam_ID']);
            if(!empty($items) and $items['success']) {
                $descriptionHash = [];
                foreach ($items['descriptions'] as $desc) {
                    $key = $desc['classid'].'_'.$desc['instanceid'];
                    $descriptionHash[$key] = $desc;
                }

                foreach($items['assets'] as $key=>$item) {
                    $key = $item['classid'].'_'.$item['instanceid'];
                    $description = $descriptionHash[$key];
                    if($description['tradable']){
                        $import = \Lucky\Models\Item::findOneBy([
                            "classid"=>$description['classid'],
                            "instanceid"=>$description['instanceid']
                        ]);
                        if(!$import) $import = new \Lucky\Models\Item();

                        $import->name = $description['name'];
                        $import->fake = false;
                        $import->amount += $item['amount'];
                        $arPrice = $steamApi->getInventoryPrice($description['market_hash_name']);
                        if(!$arPrice['lowest_price'] || (float)str_replace(',','.',$arPrice['lowest_price']) == 0){
                            $price = 0;
                        }else{
                            $price = (float)str_replace(',','.',$arPrice['lowest_price']);
                            $price = round($price*0.8, 2);
                        }
                        $import->price = $price;
                        $import->botName = $oneBot['username'];
                        $import->assetid = $item['assetid'];
                        $import->classid = $item['classid'];
                        $import->instanceid = $item['instanceid'];
                        $import->market_hash_name = $description['market_hash_name'];
                        $import->icon_url = $description['icon_url'];
                        $import->save();
                    }
                }
            }else{
                $error .= _t('Ошибка при получении предметов от ').$oneBot['username'].'<br>';
            }
        }

        if ($error) {
            $status = 'error';
            $message = $error;
        } else {
            $status = 'info';
            $message = _t('Обновление завершилось успешно');
        }
        
        set_flash($status , $message);
        redirect('admin/item-list');
    }
    
    public function item_edit($id)
    {
        $item = \Lucky\Models\Item::findOrCreate($id);

        $form = new \Lucky\Forms\Item($item);
        $form->fieldset(_t('Введите необходимые поля'));

              
        if($form->validate()) {
            $steamApi = new SteamApi();
            $form->fill($item);
            $steamItem = $steamApi->getItemByClassId($form->values['classId'], $form->values['instanceid']);
            $item->name = $steamItem['name'];
            $item->classid = $steamItem['classid'];
            $item->instanceid = $steamItem['instanceid'] ? $steamItem['instanceid'] : $form->values['instanceid'];                    
            $item->market_hash_name = $steamItem['market_hash_name'];
            $item->icon_url = $steamItem['icon_url'];
            $item->save();
            set_flash('info',_t('Successfully saved'));
            redirect('admin/item-list');
        } 
            
        $this->data['form'] = $form->get();
        $this->data['title'] = ($id) ? _t("Редактирование предмета") : _t("Создание предмета");
        $this->view('cms/base-edit');
    }
    
    public function item_delete($id) {
        $item = \Lucky\Models\Item::find($id);

        $item->deleteItem();
        redirect('admin/item-list');
    }
    
    public function box_list() {
        
        foreach(\Lucky\Models\Category::findAll() as $category) $categories[$category->name] = $category->id;
        $filter_form = new \CMS\FilterForm();
        $filter_form->text('id')                    
                    ->text('name')
                    ->select('category', '', $categories)
                    ->text('price')
                    ->text('drop_rate');
        
        $criteria = [];
        if ($filter_form->validate()) {
            $id = $filter_form->values['id'];
            if ($id) $criteria['id'] = $id;            
            
            $name = trim($filter_form->values['name']);
            if ($name) $criteria['name'] = "LIKE %".$name."%";
            
            $price = trim($filter_form->values['price']);
            if ($price) $criteria['price'] = "LIKE %".$price."%";
            
            $category = trim($filter_form->values['category']);
            if ($category) $criteria['category'] = $category;
            
            $drop_rate = trim($filter_form->values['drop_rate']);
            if ($drop_rate) $criteria['drop_rate'] = $drop_rate;
        } 
        
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'id';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';
        
        $query = \Lucky\Models\Box::findByQuery($criteria, $_GET['sort_by']." ".$_GET['sort_order']);
        
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
        
        $this->data['filter_form'] = $filter_form;
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        
        $this->data['fields']['id'] = _t('id');
        $this->data['fields']['image'] = _t('Изображение');
        $this->data['fields']['name'] = _t('Название');
        $this->data['fields']['category'] = _t('Категория');
        $this->data['fields']['price'] = _t('Стоимость');
        $this->data['fields']['items'] = _t('Предметы');
        $this->data['sort_fields'] = ['id', 'name', 'category', 'price', 'drop_rate'];
        $this->data['page_actions']['admin/box-edit'] = _t('Добавить новый');
        $this->data['item_actions']['admin/box-edit'] = _t('edit');
        $this->data['item_actions']['admin/box-delete'] = _t('удалить');
            
        $this->data['field_filters']['items'] = function($val) {
            $imgs = '';
            foreach($val as $v) {
                 $imgs.=($v->getField('icon_url')) ? 
                     "<div style='text-align: center; display: inline-block;'>
                     <img style='max-width: 50%;' src='http://cdn.steamcommunity.com/economy/image/".$v->getField('icon_url')."'><br>
                     ".$v->getField('name')."
                     </div>
                     " : "" ;
            }
            return $imgs;
        };     
        $this->data['field_filters']['category'] = function($val) {
            return $val->getField('name');
        };
        $this->data['field_filters']['image'] = function($val) {
            if($val){
                $box = \Lucky\Models\Box::findOneBy(array('image'=>$val));
                return '<img src="'.$box->getImageUrl().'">';
            }else{
                return '';
            }
            
        };
        $this->data['title'] = _t('Список сундуков');
        $this->view('cms/base-list');
    }
    
    public function category_list() {
        
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'id';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';
        
        $query = \Lucky\Models\Category::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);
        
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['id'] = _t('id');
        $this->data['fields']['name'] = _t('Название');
        $this->data['item_actions']['admin/category-edit'] = _t('edit');
        $this->data['sort_fields'] = ['id', 'name'];
        
        $this->data['page_actions']['admin/category-edit'] = _t('Создать новую');
        
        $this->data['title'] = _t('Список категорий');
        $this->view('cms/base-list');
    }
    
    
        
    public function category_edit($id) {
        
        $category = \Lucky\Models\Category::findOrCreate($id);
        
        $form = new \Bingo\Form;
        $form->fieldset(_t('Введите необходимые поля'));
        $form->text('name', _t('Название'), 'required', $category->name);
        $form->submit(_t('Сохранить'));
              
        if($form->validate()) {
            $form->fill($category);    
            $category->save();
            set_flash('info',_t('Successfully saved'));
            redirect('admin/category-list');
        } 
            
        $this->data['form'] = $form->get();
        $this->data['title'] = ($id) ? _t("Редактирование категории") : _t("Создание категории"); 
        $this->view('cms/base-edit');
    }
    
    
    
    public function box_edit($id) {
        if (isset($_FILES['image-for-box'])) {
            $response = $this->uploadTempFile($_FILES['image-for-box'], [$this, 'saveTempImage'], [$this, 'getTempFileUrl']);
            echo json_encode($response);
            exit;
        }
        
        foreach(\Lucky\Models\Category::findAll() as $category) $categories[$category->name] = $category->id;
        $items = \Lucky\Models\Item::findBy(['box' => [NULL, $id], 'deleteAt'=>NULL]);
        $box = \Lucky\Models\Box::findOrCreate($id);
        $form = new \Bingo\Form;
        $form->fieldset(_t('Введите необходимые поля'));
        $form->text('name', _t('Название'), 'required', $box->name);
        $form->hidden('image-box', $box->image);
        $form->select('category', _t('Категория'), $categories, '', (isset($box->category->id)) ? $box->category->id: "");
        $form->text('price', _t('Стоимость'), 'required', $box->price);
        $form->html("<div class='admin-box-items'><h3>"._t("Предметы")."</h3>");
        foreach($items as $key=>$val) {
            $checked = (isset($val->box->id)) ? "checked" : "";
            $form->html("<label class='box-item-block ".($val->fake ? 'fake' : '')."'>");
            $form->html("<input type='checkbox' name='items[".$val->id."] value='".$val->id."' ".$checked.">");
            $form->html("<div class='img-wrapper'><img src='http://cdn.steamcommunity.com/economy/image/".$val->icon_url."'></div>");
            $form->html("<span>".$val->name."</span>");
            $form->html("</label>");
        }
        $form->html("</div>");
        $form->submit(_t('Сохранить'));
              
        if($form->validate()) {
            $form->fill($box); 
            $box->category = \Lucky\Models\Category::find($box->category);
            $box->setImage($form->values['image-box']);
            $box->save();
            foreach($items as $item) {
                $item->box = NULL;
                $item->save(false);
            }
            foreach(\Lucky\Models\Item::findBy(['id' => array_keys($_POST['items'])]) as $obj) {
                $obj->box = $box;
                $obj->save(false);
            }
            $this->em->flush();
            set_flash('success',_t('Successfully saved'));
            redirect('admin/box-list');
        }
        $this->data['image_url'] = $box->getImageUrl();
        $this->data['form'] = $form->get();
        $this->data['title'] = ($id) ? _t("Редактирование сундука") : _t("Создание сундука"); 
        $this->view('cms/box-edit');
    }    
    
    public function box_delete($id) {
        $box = \Lucky\Models\Box::find($id);
        foreach(\Lucky\Models\Item::findBy(['box' => $id]) as $item) {
                $item->box = NULL;
                $item->save();
        }
        $box->delete();
        redirect('admin/box-list');
    }
    
    private function validateImage($uploadedFile) {
        $error = false;
        if ($uploadedFile['error']) {
            $error = _t('Не удалось загрузить файл. Пожалуйста, попробуйте еще раз');
        } elseif (!in_array($uploadedFile['type'], ['image/png', 'image/jpeg', 'image/pjpeg'])) {
            $error = _t('Изображение должно быть формата PNG или JPG (JPEG)');
        } elseif ($uploadedFile['size'] > 1024 * 1024 * 2) {
            $error = _t('Размер файла не должен превышать 2 МБ');
        }
        return $error;
    }
    
    private function uploadTempFile($uploadedFileInfo, $saveCallable, $getUrlCallable) {
        $response = [];
        $error = $this->validateImage($uploadedFileInfo);
        if (!$error) {
            $hostObj = $saveCallable[0];
            $methodName = $saveCallable[1];
            $params = [$uploadedFileInfo['name'], $uploadedFileInfo['tmp_name']];
            $filename = call_user_func_array([$hostObj, $methodName], $params);
            if (!$filename)
                $error = _t('Не удалось сохранить файл. Попробуйте еще раз позже');
            else {
                $hostObj = $getUrlCallable[0];
                $methodName = $getUrlCallable[1];
                $params = [$filename];
                $response = ['success' => 1, 'filename' => $filename, 'url' => call_user_func_array([$hostObj, $methodName], $params)];
            }
        }

        if ($error) {
            $response = ['error' => $error];
        }
        return $response;
    }
    
    private function saveTempImage($filename, $filepath) {
        $publicTemp = '/upload/temp/';
        $path = INDEX_DIR.$publicTemp;
        if (!file_exists($path)) mkdir($path, 0755, true);
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $filename = uniqid();
        $resultFilename = $filename . '.' . $extension;
        move_uploaded_file($filepath, INDEX_DIR.$publicTemp.$resultFilename);
        return $publicTemp . $resultFilename;
    }
    
    private function getTempFileUrl($filepath, $w=150, $h=150) {
        return \Bingo\ImageResizer::get_file_url(INDEX_DIR.$filepath ,$w,$h);
    }
}