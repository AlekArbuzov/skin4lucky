<?php

namespace Lucky\Controllers\Admin;

use Lucky\Models\Page;

class Pages extends Main
{
    public function page_list(){
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'id';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';

        $query = Page::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);

        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);

        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['id'] = 'id';
        $this->data['fields']['slug'] = _t('Путь');
        $this->data['fields']['title'] = _t('Заголовок');
        $this->data['fields']['text'] = _t('Текст');
        $this->data['field_filters']['text'] = function($val, $obj) {
            return mb_substr(strip_tags($val),0,200).'...';
        };
        $this->data['item_actions']['admin/page-edit'] = _t('Изменить');
        $this->data['item_actions']['admin/page-delete'] = _t('Удалить');
        $this->data['sort_fields'] = ['id', 'name'];

        $this->data['page_actions']['admin/page-edit'] = _t('Создать новую');

        $form = new \Bingo\Form;
        $this->data['form'] = $form->get();
        $this->data['title'] = _t("Страницы");
        $this->view('cms/base-list');

    }

    public function page_edit($id){
        $page = Page::findOrCreate($id);
        $form = new \Bingo\Form;
        $form->fieldset(_t('Введите необходимые поля'));
        $form->text('title', _t('Заголовок'), 'required', $page->title);
        $form->text('slug', _t('Slug'), 'required', $page->slug);
        $form->textarea('text', _t('Текст'), 'required', $page->text)->add_class("tinymce");
        $form->submit(_t('Сохранить'));

        if($form->validate()) {
            $form->fill($page);
            $page->save();
            set_flash('info',_t('Successfully saved'));
            redirect('admin/page-list');
        }

        $this->data['form'] = $form->get();
        $this->data['title'] =  ($id) ? _t("Изменить страницу") : _t("Создать страницу");
        $this->view('cms/base-edit');
    }

    public function page_delete($id) {
        $page = Page::find($id);
        $page->delete();
        redirect('admin/page-list');
    }
}