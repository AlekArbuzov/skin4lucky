<?php

namespace Lucky\Controllers\Admin;

class PartnerProgram extends \CMS\Controllers\Admin\BasePrivate {

    public function partner_type_list() {
        
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'level';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'ASC';
        
        $query = \Lucky\Models\PartnerType::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['image'] = _t('Изображение');
        $this->data['fields']['level'] = _t('Уровень');
        $this->data['fields']['sum'] = _t('Сумма для активации');
        $this->data['fields']['precent'] = _t('Процент');
        $this->data['fields']['receive'] = _t('Refferral receive');
        
        $this->data['field_filters']['image'] = function($val,$obj) {            
            return '<img src="'.$obj->getAvatarUrl().'" class="partner-type-img">';
        };
        
        $this->data['item_actions']['admin/partner-program/partner-type-edit'] = _t('изменить');
        $this->data['page_actions']['admin/partner-program/partner-type-edit'] = _t('Создать новый');
        $this->data['sort_fields'] = ['id', 'level', 'sum','precent'];
        $this->data['title'] = _t("Список уровней"); 
        $this->view('cms/base-list');
    }
    
    public function partner_type_edit($id) {
        
        $partnerType = \Lucky\Models\PartnerType::findOrCreate($id);
        
        $form = new \Bingo\Form;
        $form->fieldset('Введите необходимые поля');
        $form->text('level', _t('Уровень'), 'required', $partnerType->level);
        $form->text('sum', _t('Сумма для активации'), 'required', $partnerType->sum);
        $form->text('precent', _t('Процент'), 'required', $partnerType->precent);
        $form->text('receive', _t('Refferral receive'), 'required', $partnerType->receive);
        $form->text('image',false,'', $partnerType->image)->add_class("browse_file image");
        $form->submit(_t('Сохранить'));
              
        if($form->validate()) {
            $form->fill($partnerType);    
            $partnerType->save();
            set_flash('info',_t('Успешное сохранение'));
            redirect('admin/partner-program/partner-type-list');
        } 
            
        $this->data['form'] = $form->get();
        $this->data['title'] = ($id) ? _t("Редактирование уровня") : _t("Создание уровня"); 
        $this->view('cms/base-edit');
    }

    public function referral_list(){
        
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'id';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';
        
        $query = \Lucky\Models\Referral::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['id'] = _t('id');
        $this->data['fields']['user'] = _t('Кто привел');
        $this->data['fields']['referral'] = _t('Кого привел');
        $this->data['fields']['date'] = _t('Дата');
        $this->data['field_filters']['user'] = function($user) {
            return $user->name;
        };
        $this->data['field_filters']['referral'] = function($referral) {
            return $referral->name;
        };
        $this->data['sort_fields'] = ['id', 'user', 'referral','date'];
        $this->data['title'] = _t("Список рефералов"); 
        $this->view('cms/base-list');
    }

    public function settings() {
        $settings = \CMS\Models\Option::get('partner_program_settings') ?: ['registration_bonus_amount' => 0, 'sale_share' => 0];

        $form = new \Bingo\Form;
        $form->text('registration_bonus_amount', _t('Размер бонуса при регистрации'), ['required', 'numeric', 'positive'], $settings['registration_bonus_amount']);
        $form->text('sale_share', _t('Процент от пополнения'), ['required', 'numeric', function($val) {
            if ($val > 100)
                throw new \ValidationException(_t('Недопустимое значение для процента'));
            return $val;
        }], $settings['sale_share']);
        $form->submit(_t('Сохранить'));
              
        if($form->validate()) {
            \CMS\Models\Option::set('partner_program_settings', $form->values);

            set_flash('info',_t('Успешное сохранение'));
            redirect('admin/partner-program/settings');
        } 
            
        $this->data['form'] = $form->get();
        $this->data['title'] = _t('Настройки партнерской программы'); 
        $this->view('cms/base-edit');
    }

    public function bonus_list(){
        
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'id';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';
        
        $query = \Lucky\Models\PartnerBonus::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['id'] = _t('ID');
        $this->data['fields']['user'] = _t('Получатель');
        $this->data['fields']['amount'] = _t('Размер бонуса');
        $this->data['fields']['type'] = _t('Тип бонуса');
        $this->data['fields']['created'] = _t('Дата');
        
        $this->data['field_filters']['user'] = function($val, $obj) {
            return $obj ? $obj->user->login : '';
        };

        $this->data['field_filters']['type'] = function($val) {
            $labels = [
                \Lucky\Models\PartnerBonus::TYPE_REGISTRATION => 'Бонус за регистрацию',
                \Lucky\Models\PartnerBonus::TYPE_SALE => 'Бонус за продажу'
            ];

            return $labels[$val];
        };
        
        $this->data['sort_fields'] = ['id', 'created', 'amount', 'type'];
        $this->data['title'] = _t("Список бонусов партнерской программы"); 
        $this->view('cms/base-list');
    }

}