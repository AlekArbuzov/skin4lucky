<?php

namespace Lucky\Controllers\Admin;

class Promocodes extends \CMS\Controllers\Admin\BasePrivate {

    public function promolist() {
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'id';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';
        
        $query = \Lucky\Models\Promocode::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields'] = [
            'id' => _t('ID'),
            'code' => _t('Код'),
            'amount' => _t('Сумма'),
            'created' => _t('Создан'),
            'user' => _t('Кем использован')
        ];

        $this->data['field_filters']['user'] = function($val, $obj) {
            if ($val) return $obj->user->login;
            return $val;
        };

        $this->data['sort_fields'] = ['id', 'code', 'amount', 'created'];
        $this->data['page_actions']['admin/promocodes/generate'] = _t('Сгенерировать новые');
       
        $this->data['title'] = _t("Промокоды"); 
        $this->view('cms/base-list');

    }
    
    public function generate(){
        $form = new \Bingo\Form;
        $form->text('count', 'Количество промокодов', ['required', 'numeric', 'positive', function($val) {
            if ($val > 1000)
                throw new \ValidationException(_t('Максимально можно сгененрировать 1000 промокодов за раз'));
            return $val;
        }]);
        $form->text('amount', 'Сумма промокода', ['required', 'numeric', 'positive']);
        $form->submit(_t('Сгенерировать'));
              
        if ($form->validate()) {
            for ($i = 0; $i < $form->values['count']; $i++) {
                $promocode = new \Lucky\Models\Promocode;
                $promocode->amount = $form->values['amount'];
                $promocode->code = \Lucky\Models\Promocode::generateCode();
                $promocode->save(false);
            }
            $this->em->flush();

            set_flash('info',_t('Промокоды сгенерированы успешно'));
            redirect('admin/promocodes/promolist');
        }
        
        $this->data['form'] = $form->get();
        $this->data['title'] =  _t('Генерация промокодов');
        $this->view('cms/base-edit');
    }
}