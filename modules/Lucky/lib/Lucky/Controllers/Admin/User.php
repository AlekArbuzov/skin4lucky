<?php

namespace Lucky\Controllers\Admin;
use Lucky\SteamApi;

class User extends Main
{
    public function user_list() {
        
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'id';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';
        
        $query = \Lucky\Models\User::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);
        
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['id'] = _t('id');
        $this->data['fields']['steam_id'] = _t('steam_id');
        $this->data['fields']['money'] = _t('Деньги');
        $this->data['fields']['name'] = _t('Имя');
        $this->data['item_actions']['admin/user-edit'] = _t('изменить');
        $this->data['sort_fields'] = ['id', 'steam_id', 'name'];
        $this->data['title'] = _t('Список пользователей');
        $this->view('cms/base-list');
    }
    
    public function user_edit($id){
        $user = \Lucky\Models\User::findOrCreate($id);
        $form = new \Bingo\Form;
        
        $form->fieldset(_t('Введите необходимые поля'));
        $form->text('name', _t('Ник'), 'required', $user->name);
        $form->text('money', _t('Деньги'), 'required', $user->money);
        $form->text('tradeLink', _t('Ссылка на трейд'), 'required', $user->tradeLink);
        $form->submit(_t('Сохранить'));

        if($form->validate()) {
            $form->fill($user);
            $user->save();
            set_flash('info',_t('Успешное сохранение'));
            redirect('admin/user-list');
        }

        $this->data['form'] = $form->get();
        $this->data['title'] = ($id) ? _t("Редактирование пользователя") : _t("Создание пользователя"); 
        $this->view('cms/base-edit');
    }
    
    public function sales_list(){
        
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'id';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';
        
        $query = \Lucky\Models\Transaction::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);
        
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
              
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['id'] = _t('id');
        $this->data['fields']['created'] = _t('Дата');
        $this->data['fields']['user'] = _t('Ник');
        $this->data['fields']['price'] = _t('Сумма');
        $this->data['fields']['type'] = _t('Операция');
        $this->data['field_filters']['user'] = function($user) {
            return $user->name;
        };
        $this->data['field_filters']['created'] = function($created) {
            return $created->format("m/d/Y");
        };
        $this->data['sort_fields'] = ['id', 'user', 'referral','date'];
        $this->data['title'] = _t("Список пополнения"); 
        $this->view('cms/base-list');
    }
    
    public function drops_list(){
        $_GET['sort_by'] = @$_GET['sort_by'] ?: 'createdAt';
        $_GET['sort_order'] = @$_GET['sort_order'] ?:'DESC';
        
        $query = \Lucky\Models\Drops::findByQuery([], $_GET['sort_by']." ".$_GET['sort_order']);
        
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields']['user'] = _t('Имя');
        $this->data['fields']['item'] = _t('Предмет');
        $this->data['fields']['box'] = _t('Сундук');
        $this->data['fields']['status'] = _t('Статус');
        $this->data['fields']['createdAt'] = _t('Дата');
        
        $this->data['field_filters']['status'] = function($val,$obj) {
            return $obj->getStatusLabel();
        };
        
        $this->data['sort_fields'] = ['user', 'item', 'box', 'status', 'createdAt'];
               
        $this->data['title'] = _t('Список категорий');
        $this->view('cms/base-list');
    }

    public function youtuber_account_links_edit() {
        $account_link_settings = \CMS\Models\Option::get('youtuber_account_links') ?: [];

        $form = new \Bingo\Form;
        $form->form_list(
            'account_links', 
            \Form::create()->text('account_link', _t('Ссылка на профиль steam'), ['required', function($val) {
                if (!preg_match('/^(?:http|https):\/\/steamcommunity.com\/id\/(.+)/', $val))
                    throw new \ValidationException(_t('Неверный формат ссылки'));
                return $val;
            }]),
            $account_link_settings,
            true
        );
        $form->submit(_t('Сохранить'));

        if($form->validate()) {
            \CMS\Models\Option::set('youtuber_account_links', $form->values['account_links']);
            set_flash('info',_t('Успешное сохранение'));
            redirect(\Bingo\Routing::$uri);
        }

        $this->data['form'] = $form->get();
        $this->data['title'] = _t("Редактирование ссылок на аккаунты youtube'ров"); 
        $this->view('cms/base-edit');
    }
}