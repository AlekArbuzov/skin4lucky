<?php

namespace Lucky\Controllers;

use Lucky\Models\User;
use Lucky\SteamApi;

class Base extends \Bingo\Controller {

    /**
     * @var User
     */
    public $user;

    protected $steamApi;

    public function __construct() {
        $this->steamApi = new SteamApi();
        $this->user = $this->data['user'] = \Lucky\Models\User::checkLoggedIn();
        $this->data['item_drops'] = \Lucky\Models\Drops::findBy([], ['createdAt DESC'],  0 , 6);
        $this->data['count_drops'] = \Lucky\Models\Drops::getCountDrops();
        $this->data['admin_email'] = \Bingo\Config::get('config','admin_email');

        parent::__construct();
    }
}