<?php

namespace Lucky\Controllers;

use Bingo\Config;
use Lucky\Models\AutomatedPurchase;
use Lucky\Models\Bot;
use Lucky\Models\Drops;
use Lucky\Models\Item;
use Lucky\CsGoApi;

class Cron extends \Bingo\Controller
{
    public function run($cronName) {
        set_time_limit (600);

        $lock_dir = INDEX_DIR."/cache/cron_locks";
        if (!file_exists($lock_dir)) mkdir($lock_dir);

        $locked_file = @fopen($lock_dir."/".$cronName,"w+");

        if (!$locked_file) { echo "can't open file"; die(); }
        if (!flock($locked_file, LOCK_EX | LOCK_NB)) { echo "cron is busy"; die(); }

        register_shutdown_function(function () use ($locked_file){ 
            flock($locked_file, LOCK_UN);
            fclose($locked_file);
        });

        if (method_exists($this, $cronName)) {
            $this->$cronName;
        }
    }

    public function sendDrops() {
        $configBots = Config::get('config','bots');
        $bot = new Bot($configBots);
        $drops = Drops::getByStatus(Drops::STATUS_SENDING);
        /** @var Drops $drop */
        foreach ($drops as $drop){
            if(!$drop->tradeofferid){
                $bot->sendTrade($drop);
            }
        }
        $bot->confirmationTrades();
    }

    public function makeAutoPurchase() {

        $items = $this->em->createQuery("
            SELECT i
            FROM \Lucky\Models\Item i
            WHERE i.amount < i.minStock AND i.fake = false
        ")->getResult();

        foreach ($items as $item) {
            $csGoApi = new \Lucky\CsGoApi($item->botName);
            /** Проверяем кол-ве в ожидании на получение*/
            $amountOnHold = AutomatedPurchase::getHoldItemCount($item->id);
            if(($item->amount + $amountOnHold) >= $item->minStock) continue;

            $itemInfo = $csGoApi->getInfoSaleItem($item->classid, $item->instanceid);

            if (!$itemInfo) {
                trigger_error("CsGo API unavailable or item doesn't exists");
                continue;
            }

            $itemPrice = 0;
            $itemCount = 0;
            foreach ($itemInfo->offers as $offer) {
                $itemPrice = $offer->price;
                $itemCount += $offer->count;

                if ($itemCount >= $item->autoPurchaseAmount)
                    break;
            }

            $availableCount = min($itemCount, $item->autoPurchaseAmount);
            for ($i = 0; $i < $availableCount; $i++) {
                $result = $csGoApi->buyItem($item->classid, $item->instanceid, $itemPrice, $itemInfo->hash);

                if (!$result || $result->result != 'ok') {
                    trigger_error('Automated purchase failed. Item '.$item->name);
                    break;
                }

                $automatedPurchase = new \Lucky\Models\AutomatedPurchase;
                $automatedPurchase->foreign_id = $result->id;
                $automatedPurchase->item = $item;
                $automatedPurchase->botName = $csGoApi->botName;
                $automatedPurchase->price = $itemPrice/100;
                $automatedPurchase->save();

                usleep(400000);
            }
        }
    }

    /**
     * Получение предметов, купленных на csgo от бота
     */
    public function makeTradePurchase(){
        $configs = \Bingo\Config::get('config', 'bots');
        foreach ($configs as $config){
            if(isset($config['csgo_key'])){
                $csGoApi = new CsGoApi($config['username']);
                $tradeItems = $csGoApi->getMyItems();
                foreach ($tradeItems as $tradeItem) {
                    if ($tradeItem->ui_status == CsGoApi::PICK_UP_STATUS){
                        /** @var AutomatedPurchase $autoPurchase */
                        $autoPurchase = AutomatedPurchase::findOneBy([
                            'foreign_id' => $tradeItem->ui_id,
                            'status' => AutomatedPurchase::BUY_ITEM
                        ], 'date');

                        $response = $csGoApi->getItemTrade($tradeItem->ui_bid);

                        if(!$response || !$response->success){
                            trigger_error('Get trade of automated purchase failed. Item '.$tradeItem->i_market_name);
                            continue;
                        }
                        $autoPurchase->item->addAmount();
                        $autoPurchase->item->botName = $csGoApi->botName;
                        $autoPurchase->item->save();

                        $autoPurchase->status = AutomatedPurchase::RECEIVED_ITEM;
                        $autoPurchase->save();
                        usleep(400000);
                    }
                }
            }
        }
    }
}