<?php 

namespace Lucky\Controllers;

use Bingo\Configuration;
use Lucky\Models\Box;
use Lucky\Models\User;
use Lucky\Models\Drops as DropModel;
use Lucky\Helpers\RandomBox;

class Drops extends Base
{
    public function openCase($caseId){
        if(!isset($caseId) || !$this->user) return false;
        /** @var Box $box */
        $box = Box::find($caseId);
        if(!$box) return false;
        $random = new RandomBox($box->getRealItems());
        $resultItem = $random->getRandomItemFor($this->user);
        if($resultItem){
            /** @var User $user */
            $user = User::find($this->user->id);
            if(!$user->pickMoney($box->price)){
                $this->sendResponse('main/item-box/no-money', [], $error = true);
            }
            $user->save();
            $drop = DropModel::createDrop($user, $resultItem, $box);
            if(!$drop)
                $this->sendResponse('main/error', ['admin_email'=>$this->data['admin_email']], $error = true);
            $this->sendResponse('main/item-box/item', ['drop'=>$drop]);
        }else{
            $this->sendResponse('main/item-box/empty-box', [], $error = true);
        }
    }
    
    public function actionDropItem(){
        if(!isset($_POST['action']) ||
           !isset($_POST['drop'])||
           !$this->user
          ) return false;
        /** @var DropModel $drop*/
        $drop = DropModel::find($_POST['drop']);
        if(!$drop || $drop->user != $this->user) return false;
        $view = ($_POST['profile'] == 'true') ? 'main/_drop-status' : false;
        switch($_POST['action']){
            case 'send':
                if($drop->sendItem()){
                    $this->sendResponse($view ?:'main/item-box/send-item', ['drop'=>$drop]);
                }else{
                    $this->sendResponse($view ?:'main/error', ['drop'=>$drop,'admin_email'=>$this->data['admin_email']], $error = true);
                }
                break;
            case 'sell':
                if($drop->sellItem()){
                    $this->sendResponse($view ?:'main/item-box/sell-item', ['drop'=>$drop]);
                }else{
                    $this->sendResponse($view ?:'main/item-box/not-sell', ['drop'=>$drop,'admin_email'=>$this->data['admin_email']], $error = true);
                }
                break;
            default:
                return false;
        }
    }
    
    public function getLastDrops(){
        $lastId = @$_POST['lastId'] ? $_POST['lastId'] : 0;
        $drops = DropModel::findBy(['id >'=>$lastId],'createdAt ASC');
        $dropsArr = [];
        foreach($drops as $drop){
            if($drop->id != $lastId){
                $dropsArr[$drop->id] =  $this->view('main/left-block/one-item', ['drop'=>$drop],true);
            }
        }
        $countDrops = DropModel::getCountDrops();
        $response = [
            'drops'=>$dropsArr,
            'count'=> $countDrops
        ];
        echo json_encode($response);
        exit();
    }
    
    private function sendResponse($view, $data, $error = false){
        $html = $this->view($view, $data,true);
        $response = array(
            'error' => $error,
            'result' => true,
            'html' => $html,
            'money' =>$this->user->getMoney()
        );
        if(isset($data['drop'])){
            $response['itemId'] = $data['drop']->item->id;
        }
        echo json_encode($response);
        exit();
    }
}