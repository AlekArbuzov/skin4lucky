<?php

namespace Lucky\Controllers;


class Main extends Base
{
    
    public function index()
    {
        if(isset($_GET['ref'])){
            setcookie('referral', $_GET['ref']);
        }

        $this->data['boxes_random'] = \Lucky\Models\Box::findBy(array('category'=>2));
        $this->data['boxes'] = \Lucky\Models\Box::findBy(array('category'=>1));
        $this->view('main/index');
    }

    public function box($id)
    {
        $this->data['box'] = \Lucky\Models\Box::find($id);
        $this->view('main/box');
    }
    
    public function faq()
    {
        $faq_type = \Lucky\Models\ArticleType::findOneBy(['name'=>'Раздел FAQ']);
        $this->data['articles'] = \Lucky\Models\Articles::findBy(['article_type'=>$faq_type]);
        $this->view('main/faq');
    }
    
    public function partner()
    {
        $this->data['partnerTypes'] = \Lucky\Models\PartnerType::findBy([]);
        $this->view('main/partner');
    }
    
    public function gifts()
    {
        $this->view('main/gifts');
    }

    public function terms_of_use(){
        $this->view('main/terms');
    }
}
