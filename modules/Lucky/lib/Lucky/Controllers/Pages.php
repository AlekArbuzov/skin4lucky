<?php

namespace Lucky\Controllers;


use Lucky\Models\Page;

class Pages extends Base
{
    public function page(Page $page)
    {
        $this->data['title'] = $page->title;
        $this->data['page'] = $page;
        $this->view('main/page');
    }
}