<?php

namespace Lucky\Controllers;
use Lucky\SteamApi;
use Lucky\Helpers\UnitPay;

class Payment extends Base
{
    /**
    *Хранит настройки Яндекс кассы
    *@var array
    */
    private $settings;
    
    public function __construct() {
        $this->settings = \Bingo\Config::get('config', 'unitpay');

        parent::__construct();
    }
    
    public function purchase() {
        if (!$this->user || !$_POST['sum']) redirect('/');        
        
        $user = $this->user;

        header('Content-Type: text/html; charset=UTF-8');
        

        $sum = $_POST['sum'];

        $unitPay = new UnitPay($this->settings);

        $response = $unitPay->api('initPayment', [
            'account' => $user->id,
            'desc' => $this->settings['desc'],
            'sum' => $sum,
            'paymentType' => $this->settings['paymentType'],
            'currency' => $this->settings['currency'],
            'projectId' => $this->settings['projectId']
        ]);

        // If need user redirect on Payment Gate
        if (isset($response->result->type)
            && $response->result->type == 'redirect') {
            // Url on PaymentGate
            $redirectUrl = $response->result->redirectUrl;
            // Payment ID in Unitpay (you can save it)
            $paymentId = $response->result->paymentId;
            // User redirect
            header("Location: " . $redirectUrl);

        // If without redirect (invoice)
        } elseif (isset($response->result->type)
            && $response->result->type == 'invoice') {
            // Url on receipt page in Unitpay
            $receiptUrl = $response->result->receiptUrl;
            // Payment ID in Unitpay (you can save it)
            $paymentId = $response->result->paymentId;
            // Invoice Id in Payment Gate (you can save it)
            $invoiceId = $response->result->invoiceId;
            // User redirect
            header("Location: " . $receiptUrl);

        // If error during api request
        } elseif (isset($response->error->message)) {
            $error = $response->error->message;
            print 'Error: '.$error;
        }
    }
    
    public function processRequest(){    
        $unitPay = new UnitPay($this->settings);

        try {
            // Validate request (check ip address, signature and etc)
            $unitPay->checkHandlerRequest();

            list($method, $params) = array($_GET['method'], $_GET['params']);
            $user = \Lucky\Models\user::find($params['account']);
            // Very important! Validate request with your order data, before complete order
            if (
                $params['orderCurrency'] != $this->settings['currency'] ||
                !$user ||
                $params['projectId'] != $this->settings['projectId']
            ) {
                // logging data and throw exception
                throw new InvalidArgumentException(_t('Возникла ошибка при проверке заказа.'));
            }
            
            switch ($method) {
                // Just check order (check server status, check order in DB and etc)
                case 'check':
                    print $unitPay->getSuccessHandlerResponse(_t('Проверка успешна. Готовы к оплате.'));
                    break;
                // Method Pay means that the money received
                case 'pay':
                    $transaction = \Lucky\Models\Transaction::findOneBy(['foreign_id'=>$params['unitpayId']]);
                    if(!$transaction){
                        \Lucky\Models\Transaction::createRefillMoney($user, $params['unitpayId'], $params['payerSum'], new \DateTime('Now'));
                        $user->addMoney($params['payerSum']);
                        $user->save();
                    }
                    print $unitPay->getSuccessHandlerResponse(_t('Успешная оплата.'));
                    break;
                // Method Error means that an error has occurred.
                case 'error':
                    // Please log error text.
                    trigger_error(_t('Ошибка при оплате. Id заказа в unitpay ').$params['unitpayId']);
                    print $unitPay->getSuccessHandlerResponse(_t('Ошибка обработана.'));
                    break;
            }
        // Oops! Something went wrong.
        } catch (Exception $e) {
            print $unitPay->getErrorHandlerResponse($e->getMessage());
        }
        
        
    }
}