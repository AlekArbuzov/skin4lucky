<?php

namespace Lucky\Controllers;

use Lucky\CsGoApi;
use Lucky\SteamApi;
use Bingo\Config;

class Test extends \Bingo\Controller
{
    public function index()
    {
        $urlNodeJS = Config::get('config','nodejs_server');
        $json = @file_get_contents($urlNodeJS.'skin4lucky2');
        $data = json_decode($json, TRUE);
        _D($data);
    }

    private function acceptTrade($steam, $confirmation){
        $response = false;
        while (!$response){
            var_dump($steam->mobileAuth()->confirmations()->getConfirmationTradeOfferId($confirmation));
            $response = $steam->mobileAuth()->confirmations()->acceptConfirmation($confirmation);
            _D($response);
            if (!$response){
                $steam->mobileAuth()->refreshMobileSession();
            }
        }
    }
}
?>