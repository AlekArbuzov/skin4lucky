<?php

namespace Lucky\Controllers;



class User extends Base
{
    public function logout()
    {
        if ($this->user)
            $this->user->logout();
        redirect("/");
    }

    public function check()
    {
        if (!$_POST) return false;
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($s, true);
        if(!empty($user)) {
            
            $steamId = $this->steamApi->getSteamId($user['uid']);
            $user = \Lucky\Models\User::findOneBy(['steam_id' => $steamId]);
            
            if (is_null($user)) {
                $userData = $this->steamApi->getUserData($steamId);

                if (false === $userData) {
                    trigger_error('Steam API error. User not found');
                    redirect('/');
                }
                
                $user = new \Lucky\Models\User();
                $user->steam_id = $userData['steamid'];
                $user->login = $userData['personaname'];
                $seed = 'asdfasdfgsdfgd';
                $user->password = md5(time().$seed);
                $user->name = $userData['personaname'];
                $user->avatar = $userData['avatarfull'];
                $user->save();

                if (isset($_COOKIE['referral'])){
                    $partner = \Lucky\Models\User::findOneBy(['referralLink' => $_COOKIE['referral']]);
                    if ($partner) {
                        $refObject = new \Lucky\Models\Referral();
                        $refObject->user = $partner;
                        $refObject->referral = $user;
                        $refObject->save(false);  

                        $partnerProgramSettings = \CMS\Models\Option::get('partner_program_settings') ?: [];
                        $bonus = new \Lucky\Models\PartnerBonus;
                        $bonus->amount = !empty($partnerProgramSettings['registration_bonus_amount']) ? $partnerProgramSettings['registration_bonus_amount'] : 0; 
                        $bonus->user = $user;    
                        $bonus->type = \Lucky\Models\PartnerBonus::TYPE_REGISTRATION;
                        $bonus->save(false);

                        $user->addMoney($bonus->amount);
                        $user->save(false);

                        $this->em->flush();
                    }
                }

            }
        
            \Lucky\Models\User::loginUser($user, $sessionExpire = true);
        }

        redirect('/');
    }
    
    public function profile()
    {
        if(!$this->user){
            redirect('/');
        }
        if(isset($_POST['tradeLink'])){
            if($this->user->setTradeLink($_POST['tradeLink'])){
                $status = 'OK';
            }else{
                $status = false;
            }
            $response = array('status'=>$status);
            echo json_encode($response);
            exit();
        }
        
        $query = \Lucky\Models\Drops::findByQuery(['user'=>$this->user], 'createdAt DESC');
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $pagination = new \Bingo\Pagination(30,$page,false,false,$query);
        if($_POST){
            $html = $this->view('main/profile-items',array('drops'=>$pagination->result()),true);
            
            $response = array(
                'html' => $html,
            );
            echo json_encode($response);
            exit();
        }
        $this->data['pagination'] = $pagination->get(10);
        $this->data['drops'] = $pagination->result();
        $this->data['maxPage'] = $pagination->getPageCount();
        $this->data['articles'] = \Lucky\Models\Articles::findBy(['article_type'=>3]);
        $this->data['inventoryInfo'] = (isset($inventoryInfo['rgDescriptions'])) ? $inventoryInfo['rgDescriptions'] : null ;
        $this->view('main/profile');
    }

    public function use_promocode() {
        if (!$this->user) redirect('/');
        
        $form = new \Lucky\Forms\Promocode($this->user);
        if (isset($_POST['code'])) {
            if ($form->validate()) {
                $promocode = $form->values['code'];
                $promocode->user = $this->user;
                $promocode->save(false);

                $this->user->addMoney($promocode->amount);
                $this->user->save(false);

                $this->em->flush();               

                echo 'ok';
                exit;
            }
        }

        $this->data['promocodeForm'] = $form;
        $this->view('partials/promocode_form');
    }
    
    private function checkRefferal(User $referral){
        if(isset($_COOKIE['referral'])){
            $user = \Lucky\Models\User::findOneBy(['referralLink' => $_COOKIE['referral']]);
            if($user){
                $refObject = new \Lucky\Models\Referral();
                $refObject->user = $user;
                $refObject->referral = $referral;
                $refObject->save();                
            }
        }
    }
}