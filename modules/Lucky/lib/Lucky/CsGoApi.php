<?php
namespace Lucky;

class CsGoApi
{
    /** Вещь выставлена на продажу.*/
    const IN_SELL_STATUS = 1;
    /** Вы продали вещь и должны ее передать боту.*/
    const SOLD_STATUS = 2;
    /** Ожидание передачи боту купленной вами вещи от продавца.*/
    const WAIT_TRADE_STATUS = 3;
    /** Вы можете забрать купленную вещь.*/
    const PICK_UP_STATUS = 4;

    private $apiKey;
    private $apiUrl = "https://csgo.tm/api/";
    public $botName = false;

    public function __construct($botName = false)
    {
        $configs = \Bingo\Config::get('config', 'bots');
        $anyApiKey = false;
        $anyBot = false;
        $this->apiKey = false;
        foreach ($configs as $config){
            if(isset($config['csgo_key'])){
                $anyApiKey = $config['csgo_key'];
                $anyBot = $config['username'];
                if($config['username'] == $botName){
                    $this->apiKey = $config['csgo_key'];
                    $this->botName = $config['username'];
                    break;
                }
            }
        }
        if(!$this->apiKey && $anyApiKey){
            trigger_error("CsGo API Key not have current bot.");
            $this->apiKey = $anyApiKey;
            $this->botName = $anyBot;
        }elseif(!$anyApiKey){
            trigger_error("CsGo not have any key. AutomatedPurchase not work");
            return false;
        }
    }
    /**
    * Информация и предложения о продаже конкретной вещи.
    */
    public function getInfoSaleItem($classId, $instanseId, $language = "ru")
    {
        $url = $this->apiUrl."ItemInfo/".$classId."_".$instanseId."/".$language."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Информация о ценах и о последних 500 покупках конкретной вещи.
    */
    public function getLastPriceSaleItem($classId, $instanseId)
    {        
        $url = $this->apiUrl."ItemHistory/".$classId."_".$instanseId."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Список трейдов, которые маркет отправил вам и они активны в данный момент.
    */
    public function getActiveTrade($classId, $instanseId)
    {
        $url = $this->apiUrl."MarketTrades/".$classId."_".$instanseId."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Cписок предметов со страницы "Мои вещи".
    */
    public function getMyItems()
    {       
        $url = $this->apiUrl."Trades/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Покупка предмета.
    * $price - цена в копейках
    * $hash - необязательный параметр, передается hash из getInfoSaleItem() конкретного предмета
    */    
    public function buyItem($classId, $instanseId, $price, $hash = "")
    {
        $url = $this->apiUrl."Buy/".$classId."_".$instanseId."/".$price."/".$hash."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Выставить новый предмет на продажу.
    */
    public function toSellItem($classId, $instanseId, $price)
    {
        $url = $this->apiUrl."SetPrice/new_".$classId."_".$instanseId."/".$price."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Убрать все предметы с продажи.
    */
    public function removeAllSales()
    {
        $url = $this->apiUrl."RemoveAll/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Изменить цену выставленного на продажу предмета, или снять предмет с продажи.
    * $itemId - уникальный номер вещи, который можно найти в свойствах предмета со страницы sell (api /Trades/)
    * $price - Отправьте 0, чтобы снять предмет с продажи.
    */
    public function editPriceItemInSales($itemId, $price)
    {
        $url = $this->apiUrl."SetPrice/".$itemId."/".$price."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Если вы хотите быть онлайн на сайте, и чтобы ваши вещи продавались, вам нужно раз в 3 минуты выполнять этот скрипт.
    */
    public function beOnline()
    {
        $url = $this->apiUrl."PingPong/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * С помощью этого методы, Вы можете запросить передачу предмета который был куплен у Вас или куплен Вами. Вам будет отправлен оффлайн трейдоффер в Steam,
     * который необходимо принять в течении 2 минут. В одну операцию может попасть максимум 20 предметов.
    * $botId - При выводе купленной вещи, вы можете посмотреть этот ui_bid в свойствах предмета со страницы sell (api /Trades/).
    */
    public function getItemTrade($botId)
    {
        $url = $this->apiUrl."ItemRequest/out/".$botId."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);        
    }
    
    /**
    * Получить историю ваших операций на csgo.tm.
    * $start_time, $end_time - время, с которого выводить операции (UNIX timestamp, longint)
    */
    public function getHistory($start_time, $end_time)
    {
        $url = $this->apiUrl."OperationHistory/".$start_time."/".$end_time."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Получить список стикеров CS:GO.
    */
    public function getStickers()
    {
        $url = $this->apiUrl."GetStickers/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Получить количество денег на счету в копейках.
    */
    public function getAmount()
    {
        $url = $this->apiUrl."GetMoney/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Проверка ограничений по торговли. Если проблем нет - все параметры в статусе "true".
    */
    public function testTrade()
    {
        $url = $this->apiUrl."Test/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Статус кэша инвентаря. i_status != 0 - обновляется.
    */
    public function geStatusInventory()
    {
        $url = $this->apiUrl."InventoryStatus/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Отправить запрос на обновление инвентаря.
    */
    public function updateInventory()
    {
        $url = $this->apiUrl."UpdateInventory/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Получить установленный токен.
    */
    public function getToken()
    {
        $url = $this->apiUrl."GetToken/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Установить новый токен, который можно посмотреть в ссылке для оффлайн обменов стим.
    */
    public function setToken($newToken)
    {
        $url = $this->apiUrl."SetToken/".$newToken."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Получить список предметов для моментальной покупки.
    */
    public function getQuickBuyItems()
    {
        $url = $this->apiUrl."QuickItems/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Моментально купить предмет из метода QuickItems (За цену, которая указана в параметре "l_paid" в копейках). 
    * Через секунду его можно будет забрать через метод ItemRequest.
    * $ui_id - предмет, берем из ответа метода getQuickBuyItems()
    */
    public function quickBuy($ui_id)
    {
        $url = $this->apiUrl."QuickBuy/".$ui_id."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Получить список выставленных ордеров с личного кабинета при автозакупке.
    */
    public function getAutoOrders()
    {
        $url = $this->apiUrl."GetOrders/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Создание новой заявки на автопокупку.
    * $price - цена в копейках(целое число)
    * $hash - md5 от описания предмета. Вы можете найти его в ответе метода ItemInfo
    */
    public function createAutoOrder($classId, $instanseId, $price, $hash)
    {
        $url = $this->apiUrl."InsertOrder/".$classId."/".$instanseId."/".$price."/".$hash."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Изменение/Удаление заявки на автопокупку.
    * $price - цена в копейках(целое число), цена указанная в заявке на покупку изменится на указанную тут. Если вы пришлете 0, то эта заявка на покупку будет удалена.
    */
    public function updateAutoOrder($classId, $instanseId, $price)
    {
        $url = $this->apiUrl."UpdateOrder/".$classId."/".$instanseId."/".$price."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Удаление всех заявок на автопокупку.
    */
    public function deleteAllAutoOrder()
    {
        $url = $this->apiUrl."DeleteOrders/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Получить список включенных уведомлений о изменении цены отслеживаемого предмета.
    */
    public function getNotificationPrice()
    {
        $url = $this->apiUrl."GetNotifications/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Изменение/Удаление уведомления о изменении цены на остлеживаемый предмет.
    * $price - цена в копейках(целое число), если появится предложение о покупке ниже этой цены, то вы получите уведомление. Если вы пришлете 0, то это уведомление будет удалено.
    */
    public function updateNotification($classId, $instanseId, $price)
    {
        $url = $this->apiUrl."UpdateNotification/".$classId."/".$instanseId."/".$price."/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
    
    /**
    * Ключ для подписки на вебсокеты. Получение приватных оповещений.
    */
    public function getKeyWebSocket()
    {
        $url = $this->apiUrl."GetWSAuth/?key=".$this->apiKey;
        $json = file_get_contents($url);
        return json_decode($json);
    }
}
?>