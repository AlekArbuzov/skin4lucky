<?php

namespace Lucky\Forms;

use Bingo\Form;


class Item extends Form
{
    public function __construct($item, $method = 'post', $atts = [])
    {
        parent::__construct($method, $atts);
        
        $this
            ->fieldset(_t('Введите необходимые поля'))
            ->text('classId', _t('ClassId предмета в steam'), 'required', $item->classid)
            ->text('instanceid', _t('Instanceid предмета в steam'), 'required', $item->instanceid)
            ->text('openCount', _t('Кол-во открытий сундука для дропа'), 'required', $item->openCount)
            ->number_range('chance', _t('Вероятность выпадения %'), 'required', $item->chance, ['min'=>1, 'max'=>100])
            ->checkbox('fake', _t('Фейковый предмет'), false, $item->fake)
            ->fieldset(_t('Введите данные автозакупки'))
            ->number_range('minStock', _t('Минимальный сток'), ['numeric', 'positive'], $item->minStock)
            ->number_range('autoPurchaseAmount', _t('Кол-во закупаемых предметов'), ['numeric', 'positive'], $item->autoPurchaseAmount)
            ->submit(_t('Сохранить'));
    }
    
    public function number_range($name,$label,$rules='',$value=false,$atts=false) {
        $this->add(new FormElement_Number_Range(compact('name','label','rules','value','atts')));
        return $this;
    }

}

class FormElement_Number_Range extends \Bingo\FormElement {
    function render_element() {
        echo "<input type='number' {$this->attr('id')} {$this->attr('name')} {$this->attr('rows')} {$this->attr('required')} {$this->attr('class')} {$this->attr('min')} {$this->attr('max')} value='{$this->out('value')}'>";
    }
}