<?php

namespace Lucky\Forms;

use Bingo\Form;

class Promocode extends Form {
    public function __construct($user, $method = 'post', $atts = []) {
        $atts['action'] = 'use-promocode';
        parent::__construct($method, $atts);
        
        $this->text('code', '', ['required', function($val) use ($user) {
            if ($user->lastPromocodeTry && $user->lastPromocodeTry->getTimestamp() + 2 > time()) {
                throw new \ValidationException(_t("Слишко много попыток ввести промокод"));
            }

            $user->lastPromocodeTry = new \DateTime('now');
            $user->save();

            if (!$promocode = \Lucky\Models\Promocode::findOneBy(['code' => $val])) {
                throw new \ValidationException(_t("Промокод не существует"));
            } else {
                if ($promocode->user)
                    throw new \ValidationException(_t("Промокод уже использован"));
            }

            return $promocode;
        }], '', ['class' => 'form-control']);
    }
}