<?php

namespace Lucky\Helpers;

class RandomBox {
    
    private $items = array();
    
    public function __construct($items){
        $this->items = $items;
    }
    
    public function getRandomItemFor($user = null) {
        if (count($this->items) >= 1) {
            $youtuberLinksData = \CMS\Models\Option::get('youtuber_account_links') ?: [];
            $youtuberSteamLogins = [];
            foreach ($youtuberLinksData as $one) {
                $link = $one['account_link'];
                preg_match('/^(?:http|https):\/\/steamcommunity.com\/id\/(.*)/', $link, $matches);
                if (!empty($matches[1]))
                    $youtuberSteamLogins[] = $matches[1];
            }

            $itemsHash = [];
            $itemWeights = [];
            foreach ($this->items as $item) {
                $itemsHash[$item->id] = $item;
                $chance = 100;
                if ($item->chance) {
                    $chance = $user && in_array($user->login, $youtuberSteamLogins) ? min($item->chance * 2, 100) : $item->chance;
                }
                $itemWeights[$item->id] = $chance;
            }
            $key = \Random::getWeightedElement($itemWeights);
            return $itemsHash[$key];
        } else {
            return false;
        }
    }
}