<?php
namespace Lucky\Listener;

use Lucky\Models\Item;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class ItemListener
{
    public function preUpdate(Item $item, PreUpdateEventArgs $event){
        if ($event->hasChangedField('box')) {
             $item->addToBoxAt = new \DateTime();
        }
    }
}