<?php

namespace Lucky\Models;

use DoctrineExtensions\ActiveEntity\ActiveEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="lucky_articles_type", indexes={})
 * @Entity()
 */
class ArticleType extends ActiveEntity
{
    /**
     * @Id @Column(type="integer") @GeneratedValue(strategy="AUTO")
     **/
    public $id;

    /**
     * @Column(type="string", unique=true, length=32)
     **/
    public $name;
    
    public function __toString(){
        return $this->name;
    }
}
