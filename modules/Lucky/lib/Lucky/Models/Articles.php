<?php

namespace Lucky\Models;
use DoctrineExtensions\ActiveEntity\ActiveEntity;
use Doctrine\Common\Collections\ArrayCollection;
    
/**
 * @Table(name="lucky_articles", indexes={})
 * @Entity()
 */

class Articles extends \ActiveEntity
{    
    /**
     * @Id @Column(type="integer") @GeneratedValue(strategy="AUTO")
     **/
    public $id;

    /**
     * @Column(type="string", length=100)
     **/
    public $title;
     
    /**
     * @ManyToOne(targetEntity="ArticleType")
     * @JoinColumn(name="article_type", referencedColumnName="id")
     */
    public $article_type;

    /**
     * @Column(type="text", length=1000)
     **/
    public $text;
}
