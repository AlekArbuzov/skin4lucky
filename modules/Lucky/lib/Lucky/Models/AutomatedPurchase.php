<?php

namespace Lucky\Models;

/**
 * @Table(name="lucky_automated_purchases", indexes={})
 * @Entity
 */
class AutomatedPurchase extends \ActiveEntity
{
    /** Купили предмет */
    const BUY_ITEM = 'BUY_ITEM';
    /** Получили предмет */
    const RECEIVED_ITEM = 'RECEIVED_ITEM';
    /** Ошибка при получении */
    const ERROR_ITEM = 'ERROR_ITEM';

    /**
     * @Id @Column(type="integer") 
     * @GeneratedValue(strategy="AUTO")
     **/
    public $id;

    /**
     * @Column(type="string")
     **/
    public $foreign_id;

    /**
     * @Column(type="string")
     **/
    public $status;

    /**
     * @ManyToOne(targetEntity="Item")
     * @JoinColumn(name="item", referencedColumnName="id")
     */
    public $item;

    /**
     * @Column(type="float", precision=2)
     **/
    public $price;

    /**
     * @Column(type="datetime")
     **/
    public $date;

    /**
     * @Column(type="string", length=512)
     **/
    public $botName = false;
    
    public function __construct(){
        $this->date = new \DateTime('now');
        $this->status = self::BUY_ITEM;
    }

    public function getStatusLabel() {
        $labels = [
            'BUY_ITEM' => _t('Предмет оплачен'),
            'RECEIVED_ITEM' => _t('Предмет получен'),
            'ERROR_ITEM' => _t('Ошибка при получении'),
        ];
        return isset($labels[$this->status]) ? $labels[$this->status] : _t('Неизвестен');
    }

    public static function getHoldItemCount($itemId){
        $qb = \Bingo::$em->createQueryBuilder();
        $count =  $qb->select("COUNT(a)")
            ->from('\Lucky\Models\AutomatedPurchase', 'a')
            ->where('a.item = :item')
            ->andWhere('a.status = :status')
            ->setParameter('item', $itemId)
            ->setParameter('status', self::BUY_ITEM)
            ->getQuery()->getSingleScalarResult();
        return $count;
    }

}
