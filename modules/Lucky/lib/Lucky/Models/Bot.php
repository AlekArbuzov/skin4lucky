<?php

namespace Lucky\Models;

use Bingo\Config;
use waylaidwanderer\SteamCommunity\Enum\LoginResult;
use waylaidwanderer\SteamCommunity\MobileAuth\Confirmations\Confirmation;
use waylaidwanderer\SteamCommunity\MobileAuth\WgTokenInvalidException;
use waylaidwanderer\SteamCommunity\SteamCommunity;

class Bot{

    /**
     * @var array
     */
    private $bots = array();

    /**
     * Bot constructor.
     * @param array $configBots
     */
    public function __construct(array $configBots){
        foreach($configBots as $bot){
            $steamAccount = $this->prepareConnectSteam($bot);

            if($steamAccount->doLogin() == 'Need2FA'){
                $code = $this->get2FACode($bot['username']);
                $steamAccount->setTwoFactorCode($code);
                $steamAccount->doLogin();
            }
            $this->bots[$steamAccount->getUsername()] = $steamAccount;
        }
    }

    /**
     * @return SteamCommunity[]
     */
    public function getBots(){
        return $this->bots;
    }

    /**
     * @param array $bot config authorization
     * @return SteamCommunity
     */
    public function prepareConnectSteam($bot){
        $steam = new SteamCommunity($bot, INDEX_DIR.'/botAuth');
        return $steam;
    }

    /**
     * @param Drops $drop
     * @return boolean
     */
    public function sendTrade(Drops $drop){
        $arrConfig = $drop->user->getTradeConfig();
        if (!$arrConfig){
            $drop->setStatus(Drops::STATUS_ERROR);
            return false;
        }

        /**@var SteamCommunity $bot*/
        foreach($this->bots as $bot){
            $tradeOffers = $bot->tradeOffers();
            $tradeOffers->getTradeOffersViaAPI(true);
            $trade = $tradeOffers->createTrade($arrConfig['partner']);
            $trade->addMyItem(730, 2, $drop->item->assetid);
            $tradeofferid = $trade->sendWithToken($arrConfig['token']);

            if(!$tradeofferid) {
                trigger_error('DropID = '.$drop->id.$trade->getError());
                continue;
            }else{
                $drop->tradeofferid = $tradeofferid;
                $drop->save();
                return true;
            }
        }
        return false;
    }

    /**
     * Get Two factor authorisation code
     * from nodejs bot
     * @param string $name name steam bot account
     * @return string|false
     */
    private function get2FACode($name){
        $urlNodeJS = Config::get('config','nodejs_server');
        $json = @file_get_contents($urlNodeJS.$name);
        $data = json_decode($json, TRUE);
        return $data['code2FA'] ?:false;
    }

    public function confirmationTrades(){
        /**@var SteamCommunity $bot*/
        foreach($this->bots as $bot) {
            $confirmations = $bot->mobileAuth()->confirmations()->fetchConfirmations();
            foreach ($confirmations as $confirmation) {
                $this->acceptTrade($bot, $confirmation);
            }
        }
    }

    /**
     * @param SteamCommunity $steam
     * @param Confirmation $confirmation
     * @return boolean
     */
    private function acceptTrade($steam, $confirmation){
        $response = false;
        while (!$response){
            $tradeOfferId = $steam->mobileAuth()->confirmations()->getConfirmationTradeOfferId($confirmation);

            if(!$tradeOfferId) return false;

            /** @var Drops $drop */
            $drop = Drops::findOneBy(['tradeofferid'=>$tradeOfferId]);
            if(!$drop) return false;

            $response = $steam->mobileAuth()->confirmations()->acceptConfirmation($confirmation);
            if (!$response){
                $steam->mobileAuth()->refreshMobileSession();
            }else{
                $drop->setStatus(Drops::STATUS_SEND);
                return true;
            }
        }
    }
}