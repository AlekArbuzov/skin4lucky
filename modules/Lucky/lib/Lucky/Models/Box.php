<?php

namespace Lucky\Models;
use DoctrineExtensions\ActiveEntity\ActiveEntity;
use Doctrine\Common\Collections\ArrayCollection;
    
/**
 * @Table(name="lucky_boxes", indexes={})
 * @Entity()
 * @HasLifecycleCallbacks
 */

class Box extends \ActiveEntity
{    
    /**
     * @Id @Column(type="integer") @GeneratedValue(strategy="AUTO")
     **/
    public $id;

    /**
     * @Column(type="string", length=50)
     **/
    public $name;
    
    /**
     * @Column(type="string", length=100)
     **/
    public $image;
     
    /**
     * @ManyToOne(targetEntity="Category")
     * @JoinColumn(name="category", referencedColumnName="id")
     */
    public $category;

    /**
     * @Column(type="integer", length=8)
     **/
    public $price;
    
    /**
     * @OneToMany(targetEntity="Item", mappedBy="box")
     */
    public $items;
    
    public function __construct() {
        $this->items = new ArrayCollection();
    }
    
    public function __toString(){
        return $this->name;
    }
    
    public function getImageUrl($w = 296, $h = 296){
        $path = $this->getImagesPath() . $this->image;
        if ($this->image && file_exists($path))
            return \Bingo\ImageResizer::get_file_url($path,$w,$h);
        else 
            return false;
    }
    
    public function getPrice(){
        return number_format($this->price, 2, ',', ' ');
    }
    
    public function setImage($file) {
        $filename = uniqid('avatar_') . '.' . pathinfo(INDEX_DIR.$file, PATHINFO_EXTENSION);
        $resultFilename = $this->getImagesPath() . $filename;
        if(rename(INDEX_DIR.$file, $resultFilename)) {
            $oldAvatar = $this->getImagesPath() . $this->image;
            if ($this->image && file_exists($oldAvatar))
                unlink($oldAvatar);
            $this->image = $filename;
            return $this->getImageUrl();
        }
        return false;
    }
    
    public function getImagesPath() {
        $shortClassName = explode("\\", get_class($this));
        $shortClassName = strtolower(array_pop($shortClassName));
        
        $path = INDEX_DIR . '/upload/'.$shortClassName.'_images/' . $this->id . '/';
        if (!file_exists($path)) 
             mkdir($path, 0755, true);
        return $path;
    }
    
    /**
    * Get item, where amount > 0 and 
    * item is not fake
    * @return array(Lucky\Models\Item)
    */
    public function getRealItems(){
        $itemsReal = array();
        foreach($this->items as $item){
            if($item->checkDrop()){
                $itemsReal[] = $item;
            }
        }
        return $itemsReal;
    }
    
    public function checkDrops(){
        $isDrops = false;
        foreach($this->items as $item){
            if(!$item->fake && $item->amount >= 1){
                $isDrops = true;
            }
        }
        return $isDrops;
    }
}
