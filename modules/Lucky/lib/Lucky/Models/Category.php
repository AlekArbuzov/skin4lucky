<?php

namespace Lucky\Models;

use DoctrineExtensions\ActiveEntity\ActiveEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="lucky_categories", indexes={})
 * @Entity()
 */
class Category extends ActiveEntity
{
    /**
     * @Id @Column(type="integer") @GeneratedValue(strategy="AUTO")
     **/
    public $id;

    /**
     * @Column(type="string", unique=true, length=32)
     **/
    public $name;

}
