<?php

namespace Lucky\Models;

use Bingo\Config;
use DoctrineExtensions\ActiveEntity\ActiveEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="lucky_drops", indexes={})
 * @Entity()
 */
class Drops extends ActiveEntity
{
    const STATUS_OPEN = 'OPEN';
    const STATUS_SENDING = 'SENDING';
    const STATUS_SEND = 'SEND';
    const STATUS_SOLD = 'SOLD';
    const STATUS_ERROR = 'ERROR';

    /**
     * @Id @Column(type="integer") @GeneratedValue(strategy="AUTO")
     **/
    public $id;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    public $user;
    
    /**
     * @ManyToOne(targetEntity="Item")
     * @JoinColumn(name="item", referencedColumnName="id")
     */
    public $item;

    /**
     * @ManyToOne(targetEntity="Box")
     * @JoinColumn(name="box", referencedColumnName="id")
     */
    public $box;

    /**
     * @Column(type="string", length=32)
     **/
    public $status;

    /**
     * @Column(type="integer", nullable=true)
     **/
    public $tradeofferid;
    
    /** @Column(type="datetime")*/
    public $createdAt;
    
    public function __construct(){
        $this->createdAt = new \DateTime('now');
    }

    public static function createDrop(User $user, Item $item, Box $box, $status = self::STATUS_OPEN) {
        if(!$item->deductAmount()){
            return false;
        }
        $drop = new Drops();
        $drop->user = $user;
        $drop->item = $item;
        $drop->box = $box;
        $drop->status = $status;
        $drop->createdAt = new \DateTime('now');

        $drop->save(false);

        \Bingo::$em->flush();
        return $drop;
    }
    
    public function getStatusLabel() {
        $labels = [
            'OPEN' => _t('Открыт'),
            'SENDING' => _t('Отправляется'),
            'SEND' => _t('Отправлен'),
            'SOLD' => _t('Продан'),
            'ERROR' => _t('Ошибка'),
        ];
        return isset($labels[$this->status]) ? $labels[$this->status] : _t('Неизвестен');
    }
    
    public function sendItem(){
        if($this->status == self::STATUS_OPEN){
            $this->setStatus(self::STATUS_SENDING);
            return true;
        }else{
            $this->status = self::STATUS_ERROR;
            $this->save();
            return false;
        }
    }


    public function setStatus($status){
        if(self::issetStatus($status)){
            $this->status = $status;
            $this->save();
            return true;
        }
        return false;
    }
    
    public function sellItem(){
        if($this->status == self::STATUS_OPEN){
            /** @var User $user */
            $user = $this->user;
            if($this->item->price == 0){
                $this->status = self::STATUS_ERROR;
                $this->save();
                return false;
            }
            $user->addMoney($this->item->price);
            $user->save();
            $this->item->addAmount();
            $this->status = self::STATUS_SOLD;
            $this->save();
            return true;
        }else{
            return false;
        }
        
    }
    
    public static function getCountDrops(){
        $drops = self::findAll();
        return count($drops);
    }

    public static function getByStatus($status){
        if(self::issetStatus($status)){
            $result = self::findBy(['status'=>$status]);
            return $result;
        }
        return false;
    }

    private static function issetStatus($status){
        switch ($status){
            case self::STATUS_ERROR:
            case self::STATUS_OPEN:
            case self::STATUS_SEND:
            case self::STATUS_SENDING:
            case self::STATUS_SOLD:
                return true;
            default:
                return false;
        }
    }
}