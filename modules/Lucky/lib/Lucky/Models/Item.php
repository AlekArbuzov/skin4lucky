<?php

namespace Lucky\Models;
use DoctrineExtensions\ActiveEntity\ActiveEntity;
use Doctrine\Common\Persistence\Event\PreUpdateEventArgs;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

/**
 * @Table(name="lucky_items", indexes={})
 * @Entity()
 * @EntityListeners({"\Lucky\Listener\ItemListener"})
 */

class Item extends \ActiveEntity
{    
    /**
     * @Id @Column(type="integer") @GeneratedValue(strategy="AUTO")
     **/
    public $id;

    /**
     * @Column(type="string", length=32)
     **/
    public $name;
    
    /**
     * @Column(type="string", length=512)
     **/
    public $icon_url;
    
    /**
     * @Column(type="string", length=512, nullable=true)
     **/
    public $assetid;
    
    /**
     * @Column(type="boolean", length=512)
     **/
    public $fake = 1;
    
    /**
     * @Column(type="integer", length=20)
     **/
    public $classid;
    
    /**
     * @Column(type="integer", length=20)
     **/
    public $instanceid;
    
    /**
     * @Column(type="integer", length=20)
     **/
    public $amount = 0;
    
    /**
     * @Column(type="integer", length=3)
     **/
    public $chance = 100;
    
    /**
     * @Column(type="float", precision=2, nullable=true)
     **/
    public $price;
    
     /**
     * @Column(type="string", length=512)
     **/
    public $market_hash_name;

    /**
     * @Column(type="string", length=512)
     **/
    public $botName = false;
        
    /**
     * @ManyToOne(targetEntity="Box", inversedBy="items")
     * @JoinColumn(name="box_id", referencedColumnName="id")
     */
    public $box;
    
    /**
     * @Column(type="integer", length=20)
     **/
    public $openCount = 0;

    /**
     * @Column(type="integer")
     **/
    public $minStock = 0;

    /**
     * @Column(type="integer")
     **/
    public $autoPurchaseAmount = 0;
    
    /** 
    * @Column(type="datetime", nullable=true) 
    */
    public $addToBoxAt;
    
    /** 
    * @Column(type="datetime", nullable=true) 
    */
    public $deleteAt;
    
    public function __toString(){
        return (string)$this->name;
    }
    
    public function getImageUrl(){
        return "http://cdn.steamcommunity.com/economy/image/".$this->icon_url;
    }
    
    public function checkDrop(){
        $isDrop = true;
        if($this->amount < 1 || $this->fake || !$this->checkOpenBoxCount()){
            $isDrop = false;
        }
        return $isDrop;
    }
    
    public function deleteItem(){
        $this->deleteAt = new \DateTime();
        $this->box = NULL;
        $this->save();
    }
    
    public function deductAmount(){
        if($this->amount > 0){
//            $this->amount--;
            $this->save();
            return true;
        }
        return false;
    }

    public function addAmount($amount = 1){
        $this->amount += $amount;
        $this->save();
    }
    
    private function checkOpenBoxCount(){
        $result = false;
        if($this->openCount && $this->addToBoxAt){
            $dropsBox = \Lucky\Models\Drops::findBy(['createdAt >'=>$this->addToBoxAt, 'box'=>$this->box]);
            $count = count($dropsBox) ? count($dropsBox) : 0;
            if($count >= $this->openCount){
                $result = true;
            }
        }else{
            $result = true;
        }
        
        return $result;
    }
}
