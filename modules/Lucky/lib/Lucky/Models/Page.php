<?php

namespace Lucky\Models;

use DoctrineExtensions\ActiveEntity\ActiveEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="lucky_pages", indexes={})
 * @Entity()
 */
class Page extends ActiveEntity
{
    /**
     * @Id @Column(type="integer") @GeneratedValue(strategy="AUTO")
     **/
    public $id;

    /**
     * @Column(type="string", length=150)
     **/
    public $title;

    /**
     * @Column(type="string", unique=true, length=150)
     **/
    public $slug;

    /**
     * @Column(type="text")
     **/
    public $text;

}
