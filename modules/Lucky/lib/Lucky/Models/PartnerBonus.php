<?php

namespace Lucky\Models;

/** 
* @Entity 
* @Table(name="lucky_partner_bonus")
*/

class PartnerBonus extends \ActiveEntity {
    
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;    
    
    /** @Column(type="string") */     
    public $type;
    
    /** @Column(type="integer") */
    public $amount;
    
    /** @Column(type="datetime") */
    public $created;
    
    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    public $user;
    
    const TYPE_REGISTRATION = 'registration';
    const TYPE_SALE = 'sale';
    

    public function __construct() {
        $this->created = new \DateTime('now');
    }
}