<?php

namespace Lucky\Models;
use DoctrineExtensions\ActiveEntity\ActiveEntity;

/**
 * @Table(name="lucky_partner_type", indexes={})
 * @Entity()
 */

class PartnerType extends \ActiveEntity
{    
    /**
     * @Id @Column(type="integer") @GeneratedValue(strategy="AUTO")
     **/
    public $id;
    
    /**
     * @Column(type="string", length=100)
     **/
    public $image;
            
   /**
     * @Column(type="integer", length=5)
     **/
    public $level;
    
    /**
     * @Column(type="integer", length=5)
     **/
    public $sum;
    
    /**
     * @Column(type="integer", length=5)
     **/
    public $precent;
    
    /**
     * @Column(type="float", length=10)
     **/
    public $receive;
    
    
    public function __construct(){
    }
    
    public function getImageUrl($filename, $w = 296, $h = 296){
        $path = INDEX_DIR . '/upload/CMS/files/' . $filename;
        if ($filename && file_exists($path))
            return \Bingo\ImageResizer::get_file_url($path,$w,$h);
        else 
            return false;
    }
    
    public function getAvatarUrl($w = 150, $h = 150){
        $url = $this->getImageUrl($this->image, $w, $h);
        if (!$url)
            $url = \Bingo\ImageResizer::get_file_url(INDEX_DIR.'/assets/img/noavatar_level.png', $w, $h);
        
        return $url;
    }
}
