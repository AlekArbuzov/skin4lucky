<?php

namespace Lucky\Models;
    
/**
 * @Table(name="lucky_promocodes", indexes={})
 * @Entity
 */

class Promocode extends \ActiveEntity
{    
    /**
     * @Id @Column(type="integer") 
     * @GeneratedValue(strategy="AUTO")
     **/
    public $id;

    /**
     * @Column(type="string", length=50)
     **/
    public $code;
     
    /**
     * @Column(type="integer")
     **/
    public $amount;

    /**
     * @Column(type="datetime")
     **/
     public $created;

     /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
     public $user;

     public function __construct() {
         $this->created = new \DateTime('now');
         $this->used = false;
         $this->amount = 0;
     }

     public static function generateCode() {
         return bin2hex(openssl_random_pseudo_bytes(8));
     }
}
