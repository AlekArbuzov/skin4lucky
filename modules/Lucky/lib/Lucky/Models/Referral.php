<?php

namespace Lucky\Models;
use DoctrineExtensions\ActiveEntity\ActiveEntity;

/**
 * @Table(name="lucky_refferals", indexes={})
 * @Entity()
 */

class Referral extends \ActiveEntity
{    
    /**
     * @Id @Column(type="integer") @GeneratedValue(strategy="AUTO")
     **/
    public $id;
            
   /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    public $user;
    
    /**
     * @OneToOne(targetEntity="User")
     * @JoinColumn(name="referral", referencedColumnName="id")
     */
    public $referral;
    
    /**
     * @Column(type="date")
     **/
    public $date;
    
    
    public function __construct(){
        $this->date = new \DateTime('Now');
    }
}
