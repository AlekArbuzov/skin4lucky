<?php

namespace Lucky\Models;


/** 
* @Entity 
* @Table(name="lucky_transaction")
*/

class Transaction extends \DoctrineExtensions\ActiveEntity\ActiveEntity {
    
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;    
    
    /** @Column(type="string") */
    public $foreign_id;
      
    /** @Column(type="string") */     
    public $type;
    
    /** @Column(type="integer") */
    public $price;
    
    /** @Column(type="datetime") */
    public $created;
    
    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    public $user;
    
    /**
     * @ManyToOne(targetEntity="Box")
     * @JoinColumn(name="box", referencedColumnName="id")
     */
    public $box;
    
    const TYPE_BUY = 'BUY';
    
    public function __construct() {
        $this->type = self::TYPE_BUY;
    }
    
    public static function createRefillMoney(User $user, $foreignTransactionID, $price, \DateTime $date) {
        $transaction = new \Lucky\Models\Transaction();
        $transaction->foreign_id = $foreignTransactionID;
        $transaction->user = $user;
        $transaction->price = $price;
        $transaction->created = $date;        
        $transaction->save(false);

        $referralRelation = \Lucky\Models\Referral::findOneBy(['referral' => $user]);
        if ($referralRelation) {
            /** @var User $partner */
            $partner = $referralRelation->user;
            $partnerProgramSettings = \CMS\Models\Option::get('partner_program_settings') ?: [];
            $bonusShare = !empty($partnerProgramSettings['sale_share']) ? $partnerProgramSettings['sale_share'] : 0; 
            $bonusAmount = ceil($price * ($bonusShare / 100));

            $bonus = new \Lucky\Models\PartnerBonus;
            $bonus->amount = $bonusAmount;
            $bonus->user = $partner;    
            $bonus->type = \Lucky\Models\PartnerBonus::TYPE_SALE;
            $bonus->save(false);

            $partner->addMoney($bonus->amount);
            $partner->save(false);
        }
        
        \Bingo::$em->flush();
    }
}