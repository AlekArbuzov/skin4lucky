<?php

namespace Lucky\Models;

use DoctrineExtensions\ActiveEntity\ActiveEntity;

/**
 * @Table(name="lucky_users", indexes={})
 * @Entity()
 */
class User extends \Auth\Models\User
{
    /**
     * @Id @Column(type="integer") @GeneratedValue(strategy="AUTO")
     **/
    public $id;

    /**
     * @Column(type="string", length=20)
     **/
    public $steam_id;

    /**
     * @Column(type="string", length=128)
     **/
    public $name;
    
    /**
     * @Column(type="string", length=250, nullable=true)
     **/
    public $tradeLink;
    
    /**
     * @Column(type="string", length=10, unique=true)
     **/
    public $referralLink;
    
    /**
     * @Column(type="float", precision=2)
     **/
    public $money = 0;

    /**
     * @Column(type="string", length=256)
     **/
    public $avatar;

    /**
     * @Column(type="datetime", nullable=true)
     **/
    public $lastPromocodeTry;
    
    
    /**
     * @OneToMany(targetEntity="Drops", mappedBy="user", cascade={"persist", "remove"})
     */
    public $drops;
    
    public function __construct(){        
        if(!isset($this->referralLink)){
            $this->referralLink = $this->generateReferralLink();
        }
    }
    
    public function __toString(){
        return (string)$this->name;
    }
    
    public function getMoney(){
        return round($this->money, 2);
    }
    
    public function setMoney($amount){
        $this->money = round($amount, 2);
    }
    
    public function addMoney($amount){
        $this->money += round($amount, 2);
    }
    
    public function pickMoney($amount){
        if($this->money >= $amount){
            $this->money -= $amount;
            return true;
        }else{
            return false;
        }
    }
    
    public function getTradeConfig(){
        if($this->tradeLink){
            $arrUrl = parse_url($this->tradeLink);
            if(!isset($arrUrl['query'])) return false;
            parse_str($arrUrl['query'], $arrConfig);
            if(!isset($arrConfig['partner']) || !isset($arrConfig['token'])){
                return false;
            }
            return $arrConfig;
        }
        return false;
    }

    public function setTradeLink($link){
        $this->tradeLink = $link;
        if ($this->getTradeConfig()){
            $this->save();
            return true;
        }
        return false;
    }
    
    public function dropsCount(){
        return $this->drops->count();
    }
    
    private function generateReferralLink($length = 10){
        $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }
    
    public static function userCount(){
        return count(self::findAll());
    }

}
