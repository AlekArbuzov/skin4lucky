<?php

namespace Lucky;

use Lucky\Models\User;

class Security
{
    private $session;

    private function getSession()
    {
        if (is_null($this->session)) {
            $this->session = new \Session\SessionNamespace('skin4lucky');
        }

        return $this->session;
    }

    public function login(User $user)
    {
        $this->getSession()->user = $user;
    }

    public function getUser()
    {
        return isset($this->getSession()->user) ? $this->getSession()->user : null;
    }

    public function logout()
    {
        unset($this->getSession()->user);
    }
}
