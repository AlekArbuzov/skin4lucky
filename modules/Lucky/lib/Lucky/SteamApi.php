<?php

namespace Lucky;

class SteamApi
{
    private $apiKey;

    public function __construct()
    {
        $this->apiKey = \Bingo\Config::get('config', 'steam_key');
    }

    public function getSteamId($openidClaimedId)
    {
        return str_replace("http://steamcommunity.com/openid/id/", "", $openidClaimedId);
    }

    private function query($url, array $data = [])
    {
        $url = empty($data) ? $url : $url.'?'.http_build_query($data);

        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL,$url);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
        $query = curl_exec($curl_handle);
        curl_close($curl_handle);
        return $query;
    }

    public function getUserData($steamId)
    {
        $response = $this->query(
            "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/",
            ['key' => $this->apiKey, 'steamids' => $steamId]
        );

        $content = json_decode($response, true);

        if (!isset($content['response']['players'][0])) {
            return false;
        }

        return $content['response']['players'][0];
    }

    public function checkAuth(array $data)
    {
        $newData = [];
        foreach ($data as $key => $value) {
            $key = str_replace("openid_", "openid.", $key);
            $newData[$key] = $value;
        }
        $newData['openid.mode'] = 'check_authentication';

        $response = $this->query("http://steamcommunity.com/openid/login", $newData);

        if ($response == "ns:http://specs.openid.net/auth/2.0\nis_valid:true\n") {
            return true;
        }

        return false;
    }

    public function getInventory($steamId)
    {
        $appId = 730;
        $response = $this->query('https://steamcommunity.com/inventory/'.$steamId.'/'.$appId.'/2?l=russian');

        $content = json_decode($response, true);

        return $content;
    }

    public function getInventoryPrice($marketHashName)
    {
        $appId = 730;
        $response = $this->query('https://steamcommunity.com/market/priceoverview/?country=RU&currency=5&appid='.$appId.'&market_hash_name='.urlencode($marketHashName));
        //$response = $this->query('http://steamcommunity.com/market/priceoverview/', []);

        $content = json_decode($response, true);

        return $content ? $content : false;
    }
    
    public function getItemByClassId($classId, $instanseId, $language = "ru")
    {
        $appId = 730;
        $response = $this->query('https://api.steampowered.com/ISteamEconomy/GetAssetClassInfo/v1/?appid='.$appId.'&class_count=1&classid0='.$classId."&instanseid0=".$instanseId."&key=".$this->apiKey."&language=".$language);
        
        $content = json_decode($response, true);
        $result = $content["result"];
        return $result["success"] ? $result[$classId] : false;
    }
}
