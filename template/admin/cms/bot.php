<? include('header.php') ?>
<? startblock('content') ?>
<div class="content">
    <div class="col-md-12">
        <div class="row">
            <?php foreach($bots as $bot):?>
            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading text-center"><h3 class="title-bot"><?php echo $bot->getUsername();?></h3></div>
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item"><?php echo _t('Статус');?> <span class="badge"><?php echo $login = $bot->isLoggedIn() ? 'OK' : 'NO';?></span></li>
                            <li class="list-group-item"><?php echo _t('Баланс');?> <span class="badge"><?php echo $bot->market()->getWalletBalance();?></span></li> 
                        </ul>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</div>
<? endblock() ?>
<? include('footer.php') ?>