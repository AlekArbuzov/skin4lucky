<? include('header.php') ?>
<? startblock('content') ?>
<div class="form">
    <div class="img-thumbnail box-image">
        <img id="box-image" src="<?= $image_url?>">        
    </div>
    <a href="#box-image" class="change-image">Сменить изображение</a>
    <input type="file" class="hidden" name="image-for-box" id="image-for-box">
    <?=$form?>
</div>
<? endblock() ?>
<? include('footer.php') ?>