<? bingo_domain('cms') ?>
<? \Bingo\Action::run('admin_pre_header'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?= \Bingo\Action::filter('admin_title',array($title)) ?></title>
    <link type="text/css" rel="stylesheet" href="<?=t_url('cms/style/jquery-ui.css')?>" media="screen">
    <link type="text/css" rel="stylesheet" href="<?=t_url('cms/style/jquery-editable-select.css')?>" media="screen">
    <link type="text/css" rel="stylesheet" href="<?=t_url('cms/style/jquery-colorbox.css')?>" media="screen">
    <link type="text/css" rel="stylesheet" href="<?=t_url('cms/style/jquery-treeTable.css')?>" media="screen">
    <link type="text/css" rel="stylesheet" href="<?=t_url('cms/style/core.css')?>" media="screen">

    <script type="text/javascript">
        var base_url="<?=base_url()?>"
        var template_base = "<?=t_url('')?>";
        var locale = "<?=bingo_get_locale()?>";
        var locale_lang = "<?=_t('en')?>";
        var browse_text = "<?=_t('Browse')?>";
    </script>
    <script src="<?=t_url('cms/script/debug.js')?>" type="text/javascript" charset="utf-8"></script>

    <script src="<?=t_url('cms/script/jquery.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?php echo url('assets/js/jquery.js')?>"></script>
    <script src="<?=t_url('cms/script/jquery-json.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-cookie.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-ui.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-ui-timepicker-addon.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-editable-select.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-colorbox.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-treeTable.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-tagsinput.js')?>" type="text/javascript" charset="utf-8"></script>

    <script src="<?=t_url('cms/script/tiny_mce/jquery.tinymce.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?php echo url('assets/js/core.js')?>"></script>

    <? \Bingo\Action::run('admin_header'); ?>
    <? emptyblock('admin_header',SUPERBLOCK) ?>
</head>
<body class="admin<?if(isset($disable_menu)&&$disable_menu) echo ' without_menu'?>">
<div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=url('/admin')?>"><?=_t("Control panel")?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               <li class="dropdown">
                    <? $user = \Bingo\Routing::$controller->user; ?>
                    <? if ($user): ?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $user->login?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?=url('admin/user-profile')?>"><i class="fa fa-fw fa-user"></i>Профиль</a>
                        </li>
                        <li>
                            <a href="<?=url('/')?>" target="_blank"><?=_t("Goto website","cms")?></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <?=anchor("admin/logout",_t("Logout","cms"))?>
                        </li>
                    </ul>
                   <? endif ?>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                
                <ul class="nav navbar-nav side-nav">
                    <? if (!isset($disable_menu) || !$disable_menu): ?>
                        <?
                        function renderMenu($items) {
                            foreach ($items as $title=>$sub) {
                                if (is_array($sub)) {
                                    echo '<li><a href="javascript:;" data-toggle="collapse" data-target="#'.str_replace(' ', '_', $title).'"><i class="fa fa-fw fa-arrows-v"></i>'.$title.'<i class="fa fa-fw fa-caret-down"></i></a>';
                                    echo '<ul id="'.str_replace(' ', '_', $title).'" class="collapse">';
                                    renderMenu($sub);
                                    echo "</ul></li>";
                                } else {
                                    echo "<li><a href='".url($sub)."'>$title</a></li>";
                                }
                            }
                        }
                        ?>
                            <? renderMenu(\Admin::$menu) ?>
                    <? endif ?>                 
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <? if (!isset($heading) || $heading): ?>
                            <h1 class="page-header">
                                <? if (isset($heading)): ?>
                                    <?=$heading?>
                                <? else: ?>
                                    <?=$title?>
                                <? endif ?>
                            </h1>
                            <? if (isset($page_actions)): ?>
                                <span class="page_actions">
                                <? foreach ($page_actions as $url=>$action): ?>
                                    <?=anchor($url,$action)?>
                                <? endforeach ?>
                                </span>
                            <? endif ?>
                        <? endif ?>
                    </div>
                </div>                
                    <? if (isset($message)): ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-info alert-dismissable">
                                    <?=$message?>
                                </div>
                            </div>
                        </div>
                    <? endif ?>
                    <? if (isset($error_message)): ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-danger alert-dismissable">
                                    <?=$error_message?>
                                </div>
                            </div>
                        </div>
                    <? endif ?>
                    <? if (get_flash('error',false)): ?>
                         <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-danger alert-dismissable">
                                    <?=get_flash('error',true)?>
                                </div>
                            </div>
                        </div>
                    <? endif ?>
                    <? if (get_flash('info',false)): ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-info alert-dismissable">
                                    <?=get_flash('info',true)?>
                                </div>
                            </div>
                        </div>
                    <? endif ?>                
                <div class="row">
                    <? emptyblock('content') ?>
                