<? if (!isset($primary_key)) $primary_key = "id"; ?>
<? if (!isset($dragsort_fields)) $dragsort_fields = array(); ?>
<? global $list_id; $list_id = ((int)@$list_id)+1; ?>
<?
    if (!isset($_POST['filter']) || !isset($_POST['filter']['flag'])) {
        if (isset($_POST['with_selected']) && @$_POST['list_id']==$list_id) {
            if (isset($list_actions[$_POST['with_selected']])) {
                $action = $list_actions[$_POST['with_selected']];
                $list_function = $action['function'];
                $action_list = array();
                if (isset($_POST['list']))
                    foreach ($_POST['list'] as $key=>$data) {
                        if (isset($data['checked'])||$_POST['with_selected']==0) {
                            $action_list[$key] = $data;
                        }
                    }
                call_user_func($list_function,$action_list,$action);
            }
        }
    }

    if (@$_REQUEST['_drag_sort']) {
        $field = $_REQUEST['sort_by'];
        $old_pos = $_REQUEST['old_pos'];
        $new_pos = $_REQUEST['new_pos'];
        
        $new = $list[$new_pos]->{$field};
        if ($old_pos < $new_pos) {
            for ($i = $new_pos; $i >= $old_pos+1; $i--) 
                $list[$i]->{$field} = $list[$i-1]->{$field};
        } else {
            for ($i=$new_pos; $i<=$old_pos-1; $i++)
                $list[$i]->{$field} = $list[$i+1]->{$field};
        }
        $list[$old_pos]->{$field} = $new;
        
        $em = \Bingo\Bingo::getInstance()->em;
        $em->flush();        
        return;
    }
        
    $fields_form_hash = array();
    if (isset($list_actions[0]) && isset($list_actions[0]['form'])) $fields_form = $list_actions[0]['form'];
    if (isset($fields_form)) {
        foreach ($fields as $f=>$caption) {
            $el = $fields_form->findElement($f);
            if ($el) {
                $fields_form_hash[$f] = $el;
                $el->label = false;
            }
        }
    }
?>

<? startblock('admin_header'); ?>
<script>
    <? if (!defined('BASE_LIST_DEFINED')): define('BASE_LIST_DEFINED',1) ?>
        var form_ids = [];
        $(document).ready(function(){
            $(".check_all").change(function(){
                $(this).parents("form").find("td.check input").attr("checked",this.checked);
            });
            $(".with_selected").change(function(){
                var val = $(this).val();
                if (val!=0 && (!(val in form_ids)) ) {
                    $(this).parents('form').submit();
                    return;
                }
                $(this).parents("form").find("div.form").css("display","none");
                $(this).parents("form").find("#form_"+val).css("display","block");
            });    
            $(".cancel_action").click(function(){
                $(this).parents("form").find(".with_selected").val(0);
                $(this).parents("form").find("div.form").css("display","none");
            });

            var filter_cb = function(){
                var params = {};
                $("tr.filter,.filter_form").find("input[name],select[name],button[name]").each(function(){
                    params[$(this).attr("name").replace("filter_","")] = $(this).val();
                });
                
                $(".sort-field").each(function(){
                    if ($(this).is(".ASC,.DESC")) {
                        params["sort_by"] = $(this).attr("data-key");
                        params["sort_order"] = $(this).hasClass("DESC") ? "DESC":"ASC";
                    }
                });
                
                var str = [];
                for(var p in params) {
                    if ($.isArray(params[p])) {
                        $.each(params[p],function(){
                            str.push(p + "=" + encodeURIComponent(this));
                        });
                    } else {
                        if (p.indexOf("[")!=-1 && !params[p]) continue;
                        str.push(p + "=" + encodeURIComponent(params[p]));
                    }
                }
                var base = location.href.split("?")[0];
                var url = base+"?"+str.join("&");
                location.href = url;
            }
                
            $(".sort-field").click(function(e){
                e.preventDefault();
                var res = "ASC";
                if ($(this).hasClass("ASC")) res = "DESC";
                $(".sort-field").removeClass("ASC DESC");
                $(this).addClass(res);
                filter_cb.call(this);
            });
            
            $("tr.filter select").change(function(){
                var rel = $(this).parents("tr.filter").attr("rel");
                if (rel=="auto") {
                    filter_cb.call(this);
                }
            });
    
            $("tr.filter input").keyup(function(e){
                if (e.keyCode == 13) {
                    filter_cb.call(this);
                }
            });
            $(".filter button").click(filter_cb);
            $(".filter_form").submit(function(e){
                filter_cb();
                e.preventDefault();
                return false;
            })
        });
    <? endif ?>
    <?
        $form_ids = array();
        if (isset($list_actions)) {
            foreach ($list_actions as $key=>$action) {
                if (isset($action['form'])) {
                    $form_ids[$key] = true;
                    $action['form']->submit(_t('Save'),'submit_save')->add_class('inline');
                    $action['form']->button('cancel',_t('Cancel'))->add_class('inline cancel_action');
                }
            }
        }
    ?>
    new_ids = <?=json_encode($form_ids)?>;
    for (var key in new_ids) form_ids[key] = new_ids[key];
    
    <? if (isset($tree_parent_field)): ?>
    $(document).ready(function(){
        <?
            $js_tree_column = array_search($tree_column,array_keys($fields));
            if (isset($list_actions)) $js_tree_column += 1;
        ?>
        $("td.<?=$tree_column?>").css("padding-left",19);
        $("table.list").treeTable({
            treeColumn:<?=$js_tree_column?>,
            clickableNodeNames:true
        });
    });
    <? endif ?>
        
    <? if (in_array(@$_REQUEST['sort_by'], $dragsort_fields)): ?>
        $(function(){
            var list_id = "#tblist_<?=$list_id?>";
            var fixHelper = function(e, ui) {
                    ui.children().each(function() {
                        $(this).width($(this).width());
                    });
                    return ui;
                };
            var handler = function(event, ui){
                var old_pos = ui.item.startPos - 2; //count starts from 2 for some reason...we count from 0
                var new_pos = ui.item.index() - 2;
                if (old_pos != new_pos) {
                    $.ajax({
                        url : document.url,
                        data: {_drag_sort: 1, old_pos: old_pos, new_pos: new_pos},
                        success : function(data) {
                        },
                        error : function() {
                        }
                    });
                }
            };
            $(list_id + ".list tbody" ).sortable({
                cancel: list_id + ".list tbody tr:first-child",
                deactivate: handler,
                helper: fixHelper,
                handle: '.<?=$_REQUEST['sort_by']?>.drag-sortable',
                start: function(event, ui) {
                    ui.item.startPos = ui.item.index();
                }
            });
        });        
    <? endif ?>
    
</script>        
<? endblock() ?>

<? if (isset($filter_form_external) && $filter_form_external): ?>
    <div class="form filter_form">
        <?= $filter_form->get() ?>
    </div>
    <? unset($filter_form) ?>
<? endif ?>


<form method="POST" autocomplete="off">
<? if (isset($list_actions)): ?>
    <input value="<?=$list_id?>" type="hidden" name="list_id">
    <select name="with_selected" class="with_selected">
        <option value=0><?=_t("...with selected")?></option>
        <? foreach ($list_actions as $key=>$action): ?>
            <? if ($key): ?>
            <option value="<?=$key?>"><?=$action['title']?></option>
            <? endif ?>
        <? endforeach ?>
    </select>
    <? foreach ($list_actions as $key=>$action) if (isset($action['form'])): ?>    
        <div class="form" id="form_<?=$key?>" style="display:none">
            <fieldset>
                <legend>
                    <?=$action['title']?>
                </legend>
                <?=$action['form']->get_elements()?>
            </fieldset>
        </div>
    <? endif ?>
<? endif ?>
<? if (isset($pagination)): ?>
    <div class="pagination">
        <?=$pagination?>
    </div>
<? endif ?>
<table id="tblist_<?=$list_id?>" class="list"><tbody>
    <tr>
        <? if (isset($list_actions)): ?>
            <th><input class="check_all" type="checkbox"></th>
        <? endif ?>
        <? foreach ($fields as $key=>$caption): ?>
            <th class="<?=$key?>">
                <? if (isset($sort_fields) && (in_array($key,$sort_fields)|| isset($sort_fields[$key]))): ?>
                    <? 
                        $capts = isset($sort_fields[$key]) ? $sort_fields[$key] : array($key => $caption); 
                        $cnt = -1;
                    ?>
                    <? foreach ($capts as $capkey=>$capt): ?>
                        <?
                            $cnt++;
                            $sortBy = @$_GET['sort_by'];
                            $sortOrder = @$_GET['sort_order']?:"ASC";
                            $sortCls = "";
                            if ($sortBy==$capkey) $sortCls = $sortOrder;
                        ?>
                        <a href="#" class="sort-field <?=$sortCls?>" data-key="<?=$capkey?>">
                            <?= ($cnt==0 ? "":" / ") . $capt?>
                        </a>
                    <? endforeach ?>
                <? else: ?>
                    <?=$caption?>
                <? endif ?>
            </th>
        <? endforeach ?>
        <? if (isset($item_actions) || (isset($filter_form) && !@$filter_autosubmit)): ?>
            <th class='actions'><?=_t('actions')?></th>
        <? endif ?>
    </tr>        
    <? if (isset($filter_form)): ?>
    <tr class="filter" <? if (isset($filter_autosubmit) && $filter_autosubmit) echo 'rel="auto"'?>>
        <? if (isset($list_actions)): ?>
            <th>
                <?
                    $el = $filter_form->findElement('perPage');
                    if ($el) {
                        $name = $el->name;
                        $el->name = "filter_".$name;
                        $el->render();
                        $el->name = $name;
                    }
                ?>
            </th>
        <? endif ?>
        <? foreach ($fields as $key=>$caption): ?>
        <th class="<?=$key?>">
            <?
                $el = $filter_form->findElement($key);
                if ($el) {
                    $name = $el->name;
                    $el->name = "filter_".$name;
                    $el->render();
                    $el->name = $name;
                }
            ?>
        </th>
        <? endforeach ?>
        <? if (isset($item_actions) || (isset($filter_form) && !@$filter_autosubmit)): ?>
            <th>
                <? if ((isset($filter_form) && !@$filter_autosubmit)): ?>
                    <button type="button" class="single inline"><?=_t("filter")?></button>
                <? endif ?>
            </th>
        <? endif ?>
    </tr>
    <? endif ?>  
    <? if (count($list)):?>
    <? foreach ($list as $obj): ?>
    <tr id="node-<?=$obj->$primary_key?>" <? if (isset($tree_parent_field) && $obj->$tree_parent_field) echo "class='child-of-node-{$obj->$tree_parent_field}'"?>>
        <? if (isset($list_actions)): ?>
            <td class="check">
                <input name="list[<?=$obj->$primary_key?>][check]" type="checkbox">
                <input name="list[<?=$obj->$primary_key?>][id]" value="<?=$obj->$primary_key?>" type="hidden">
            </td>
        <? endif ?>
        <? foreach ($fields as $key=>$caption): ?>
            <?
                if (is_array($obj)) 
                    $val = @$obj[$key];
                else
                    $val = @$obj->$key;
                
                $drag_sortable = ($key == @$_GET['sort_by'] && in_array($key, $dragsort_fields));
            ?>
            <td class="<?=$key?> <?=$drag_sortable ? "drag-sortable":""?> "><?
                if (isset($field_filters) && isset($field_filters[$key])) {
                    echo call_user_func($field_filters[$key],$val,$obj);
                } 
                else {
                    if ($drag_sortable) {
                        echo "&#8661";
                    } else if (isset($fields_form_hash[$key])) {
                        $el = $fields_form_hash[$key];
                        $el->value = $val;
                        $el->name = "list[{$obj->$primary_key}][$key]";
                        $el->render();
                    } else {
                        if ($val instanceof \DateTime) 
                            echo $val->format("d.m.Y H:i");
                        else
                            echo $val;
                    }
                }
            ?></td>
        <? endforeach ?>
        <? if (isset($item_actions) || (isset($filter_form) && !@$filter_autosubmit)): ?>
        <td class="actions">
            <? if (isset($item_actions)) foreach ($item_actions as $url=>$action): ?>
                <? if (strpos($url,'{id}')==false) $url .= "/{id}";?>
                <?=anchor(str_replace("{id}",$obj->$primary_key,$url),$action)?>
            <? endforeach ?>
        </td>
        <? endif ?>
    </tr>
    <? endforeach; ?>
    </tbody></table>
    <? else: ?>
    </tbody></table>
    <div class="message"><?=_t("Nothing yet")?></div>
    <? endif ?>    
<? if (isset($fields_form)): ?>
    <button type="submit"><?
        if (isset($list_actions[0]['title'])) 
            echo $list_actions[0]['title'];
        else
            echo _t('Save list');
    ?></button>
<? endif ?>
<? if (isset($pagination)): ?>
    <div class="pagination">
        <?=$pagination?>
    </div>
<? endif ?>
</form>
