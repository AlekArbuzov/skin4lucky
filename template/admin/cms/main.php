<? include('header.php') ?>
<div id="sum_box" class="row mbl">
                            <div class="col-lg-4">
                                <div class="panel profit db mbm">
                                    <div class="panel-body">
                                        <h4 class="value">
                                            <span data-counter="" data-start="10" data-end="50" data-step="1" data-duration="0">
                                            </span> <?php echo $sum_transactions ?> <span><?=_t('руб')?>.</span></h4>
                                        <p class="description">
                                            <?=_t('Получили пользователи')?></p>
                                        <div class="progress progress-sm mbn">
                                            <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" class="progress-bar progress-bar-success"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="panel income db mbm">
                                    <div class="panel-body">
                                        <p class="icon">
                                            <i class="icon fa fa-money"></i>
                                        </p>
                                        <h4 class="value">
                                            <span><?php echo $sum_money_drops;?> </span><span><?=_t('руб')?>.</span></h4>
                                        <p class="description">
                                            <?=_t('Потратили на кейсы')?></p>
                                        <div class="progress progress-sm mbn">
                                            <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" class="progress-bar progress-bar-info"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="panel visit db mbm">
                                    <div class="panel-body">
                                        <p class="icon">
                                            <i class="icon fa fa-group"></i>
                                        </p>
                                        <h4 class="value">
                                            <span><?php echo $count_users?></span></h4>
                                        <p class="description">
                                           <?=_t('Зарегистрированных пользователей')?></p>
                                        <div class="progress progress-sm mbn">
                                            <div role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="3100" style="width: 5%;" class="progress-bar progress-bar-warning"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i><?=_t('Последние пополнения')?></h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th><?=_t('ID')?></th>
                                                <th><?=_t('Дата')?></th>
                                                <th><?=_t('Ник')?></th>
                                                <th><?=_t('Сумма')?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($last_transactions as $transaction){ ?>
                                            <tr>
                                                <td><?php echo $transaction->id?></td>
                                                <td><?php echo $transaction->created->format("m/d/Y") ?></td>
                                                <td><?php echo $transaction->user->name ?></td>
                                                <td><?php echo $transaction->price?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="<?=url('admin/sales-list')?>"><?=_t('Все транзакции')?><i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i><?=_t('Последние пользователи')?></h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th><?=_t('ID')?></th>
                                                <th><?=_t('Ник')?></th>
                                                <th><?=_t('Стим аккаунт')?></th>
                                                <th><?=_t('Трейд ссылка')?></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                        <?php foreach($last_users as $user){ ?>
                                            <tr>
                                                <td><?php echo $user->id?></td>
                                                <td><?php echo $user->name?></td>
                                                <td><?php echo $user->steam_id?></td>
                                                <td><?php echo $user->tradeLink?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="<?=url('admin/user-list')?>"><?=_t('Посмотреть всех')?><i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i><?=_t('Последние рефералы')?></h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th><?=_t('ID')?></th>
                                                <th><?=_t('Ник кто')?></th>
                                                <th><?=_t('Ник кого')?></th>
                                                <th><?=_t('Дата')?></th>
                                            </tr>
                                        </thead>
                                        <tbody>                                       
                                        <?php foreach($last_referral as $ref){ ?>
                                            <tr>
                                                <td><?php echo $ref->id?></td>
                                                <td><?php echo $ref->user->name?></td>
                                                <td><?php echo $ref->referral->name?></td>
                                                <td><?php echo $ref->date->format("m/d/Y");?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="<?=url('admin/referral-list')?>"><?=_t('Посмотреть всех')?><i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
<? include('footer.php') ?>