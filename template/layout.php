<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php if(isset($title)) echo $title; else echo 'skin4lucky';?></title>
        
    <link href= "<?= url('assets/css/bootstrap.min.css')?>" rel="stylesheet">  
    <link href= "<?= url('assets/css/style.css')?>" rel="stylesheet">
    <link href= "<?= url('assets/css/lib/font-awesome.min.css')?>" rel="stylesheet">
    
    <script src="<?= url('assets/js/jquery-3.1.0.min.js')?>"></script>
    <script src="<?= url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?= url('assets/js/app.js')?>"></script>
    <script src="<?= url('assets/js/dropsLoader.js')?>"></script>
    <script src="//ulogin.ru/js/ulogin.js"></script>
    <script> 
        var base_url = "<?=url("/")?>";
    </script>
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=url('/')?>"><img src="<?= url('assets/img/logo.png') ?>"></a>
                <div class="social-links hidden-xs">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-steam"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                </div>
                <div class="count-users hidden-xs">
                    <span><?php echo \Lucky\Models\User::userCount();?> <?php echo _t('Игроков')?></span>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?=url('/faq')?>"><i class="fa fa-language" aria-hidden="true"></i> <?php echo _t('RU');?></a>
                    </li>
                    <li>
                        <a href="<?=url('/gifts')?>"><i class="fa fa-gift" aria-hidden="true"></i> <?php echo _t('РАЗДАЧА');?></a>
                    </li>
                    <li>
                        <a href="<?=url('/faq')?>"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <?php echo _t('FAQ');?></a>
                    </li>
                    <li>
                        <a href="<?=url('/partner')?>"><i class="fa fa-money" aria-hidden="true"></i> <?php echo _t('PARTNER');?></a>
                    </li>
                    <? if ($user): ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span id="moneyAmount"><?=$user->getMoney() ?></span> <?=_t('руб')?>.
                            <span class="caret"></span> 
                        </a>
                        <ul class="dropdown-menu" id="menu1" aria-labelledby="drop1" role="menu"> 
                            <li><a type="button" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap"><?=_t('Пополнить')?>.</a></li> 
                        </ul>
                    </li> 
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" id="drop5" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <?=$user->name ?> 
                            <span class="caret"></span> 
                        </a>
                        <ul class="dropdown-menu" id="menu1" aria-labelledby="drop1"> 
                            <li><a href="<?=url('/profile')?>"><span class="glyphicon glyphicon-briefcase"></span> <?php echo _t('Профиль')?></a></li> 
                            <li role="separator" class="divider"></li> 
                            <li><a href="<?=url('/logout')?>"><span class="glyphicon glyphicon-log-out"></span> <?php echo _t('Выйти')?></a></li> 
                        </ul>
                    </li>
                    <? else: ?>
                    <li>
                        <div id="uLogin" class="ulogin-steam" data-ulogin="display=buttons;fields=steam_id;redirect_uri=https://<?php echo $_SERVER['SERVER_NAME'].url('/')?>check;mobilebuttons=1;">
                            <span data-uloginbutton = "steam"><i class="fa fa-steam"></i> <?php echo _t('Войти через Steam');?></span>
                        </div>
                    </li>
                <? endif ?>                    
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
    
<div class="container-fluid main-block">
    <div class="row-fluid">
        <div class="hidden-xs col-sm-2 col-md-1 left-side">
            <div class="online-drop-header">
                    <span class="online-drop-text"><span class="glyphicon glyphicon-time"></span>&nbsp;&nbsp;&nbsp;<?php echo _t('On-line дроп')?></span>
                </div>
            <div class="left-side-block">
                <div class="drops-items-block">
                    <?php foreach($item_drops as $drop):?>
                        <?php include partial('main/left-block/one-item')?>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="drop-info-block text-center">
                <div class="cases-open-block">
                    <span class="cases-open-count"><?php echo $count_drops;?></span>
                    <span class="cases-open-text"><?php echo _t('кейсов открыто');?></span>
                </div>
                <div class="user-online">1258 <i class="fa fa-user" aria-hidden="true"></i> <?php echo _t('Онлайн');?></div>
            </div>
        </div>
    </div>
    <div class="content-block row">
        <?php emptyblock('content'); ?>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><?php echo _t('Пополнение баланса')?></h4>
      </div>
        <form method="post" action="<?php echo url('purchase')?>">
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="control-label"><?php echo _t('Сумма в рублях')?></label>
                <input type="number" class="form-control" name="sum" id="sum">
              </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-pay-balance"><?php echo _t('Пополнить счет')?></button>
                <button class="btn btn-success use-promocode"><?php echo _t('Использовать промо-код')?></button>
            </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="promocode_popup" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo _t('Введите промо-код')?></h4>
      </div>
      <div class="modal-body">
        <div class="form">
            <?php include partial('partials/promocode_form') ?>
        </div>
        <div class="success-message hidden">
            <p><?php echo _t('Промо-код использован успешно') ?></p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="hidden">
    <div id="fountainG">
        <div id="fountainG_1" class="fountainG"></div>
        <div id="fountainG_2" class="fountainG"></div>
        <div id="fountainG_3" class="fountainG"></div>
        <div id="fountainG_4" class="fountainG"></div>
        <div id="fountainG_5" class="fountainG"></div>
        <div id="fountainG_6" class="fountainG"></div>
        <div id="fountainG_7" class="fountainG"></div>
        <div id="fountainG_8" class="fountainG"></div>
    </div>
</div>
<footer class="footer">
    <div class="container-fluid">
        <div class="col-xs-12 col-sm-5 col-md-4 text-left">
            <div class="row">
                <div class="social-links col-md-12">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-steam"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                </div>
                <div class="col-md-12 footer-logo">
                    <a href="<?php echo url('/');?>"><img src="<?php echo url('assets/img/logo.png')?>"></a>
                </div>
                <div class="col-md-12 footer-text">
                    <p><?php echo _t('Skin4Lucky internet Shop &copy; 2016 Terms of Service');?></p>
                    <p><?php echo _t('На нашем сайте вы можете открыть различные кейсы CS:GO по самым выгодным ценам.');?></p>
                    <p><?php echo _t('Все обмены проходят в автоматическом режиме с помощью ботов Steam.');?></p>
                    <p><a class="terms-link" href="<?php echo url('page/agreement');?>"><?php echo _t('Пользовательское соглашение.');?></a></p>
                </div>
                <div class="col-md-12 footer-support-mail">
                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<a href="mailto:<?php echo $admin_email;?>"><?php echo $admin_email;?></a>
                </div>
            </div>
        </div>
    </div>
</footer>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?143"></script>

    <!-- VK Widget -->
    <div id="vk_community_messages"></div>
    <script type="text/javascript">
        VK.Widgets.CommunityMessages("vk_community_messages", 67408982, {tooltipButtonText: "Проблема с сайтом ? Есть Вопросы ?"});
    </script>
</body>
</html>


