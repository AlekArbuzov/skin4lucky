<?php include partial('layout') ?>

<?php startblock('content'); ?>
<div class="col-md-12">
    <ul class="breadcrumb">
        <li><a href="<?=url('/#'.$box->category->name)?>"><?php echo $box->category->name?></a></li>
        <li class="active"><?php echo $box->name?></li>
    </ul>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 text-center case-open">
    <h3 class="case-name"><?=$box->name?></h3>
    <img id="box-img" src="<?=$box->getImageUrl()?>"/>
    <br>
    <?php if($this->user):?>
        <?php if($box->checkDrops()):?>
            <a href="#" box-id="<?php echo $box->id?>" class="btn btn-primary btn-buy"><?php echo _t('Открыть кейс за');?> <i class="fa fa-rub" aria-hidden="true"></i> <?=$box->getPrice();?></a>
        <?php else:?>
            <a href="#" box-id="<?php echo $box->id?>" class="btn btn-primary btn-buy disabled"><?php echo _t('Временно недоступен');?></a>
        <?php endif;?>
    <?php else:?>
        <a href="#" class="btn btn-primary btn-login"><?php echo _t('Войти в Steam')?></a>
    <?php endif;?>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
    <h4><?php echo _t('Содержимое кейса');?>:</h4>
    <?php foreach ($box->items as $item): ?>
    <div class="text-center image-item box-item-block">
        <div class="box-item-image" rel="<?php echo $item->id?>">
            <img src="<?=$item->getImageUrl()?>" />
        </div>
        <div class="name-item">
            <p><?=$item->name?></p>
        </div>
    </div>
    <?php endforeach ?>
</div>
<? endblock(); ?>