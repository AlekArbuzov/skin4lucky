<?php include partial('layout') ?>

<?php startblock('content'); ?>
<div class="col-md-12">
    <ul class="breadcrumb">
        <li><a href="<?=url('/')?>"><i class="fa fa-home" aria-hidden="true"></i> <?php echo _t('Главная');?></a></li>
        <li class="active"><?php echo _t('Ответы на вопросы');?></li>
    </ul>
</div>
<?php 
$count = count($articles);
$delimeter = ceil($count/2);
$i = 0;
?>
<div class="col-md-6">
<?php foreach ($articles as $article): ?>
    <?php if($i >= $delimeter){ $i = 0; ?>
    </div>
    <div class="col-md-6">
    <?php } ?>
    <div class="col-md-12 center-block article-block">
        <div class="article-header">
            <h4><i class="fa fa-hashtag" aria-hidden="true"></i>  <?php echo $article->title ?></h4>
        </div>
        <div class="article-body">
            <?php echo $article->text ?>
        </div>
    </div>
    <?php $i++;?>
<?php endforeach; ?>
</div>
<? endblock(); ?>