<?php include partial('layout') ?>

<?php startblock('content'); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main-items">
        <h4 class="header-blocks-item main-title"><i class="fa fa-crosshairs" aria-hidden="true"></i> <?php echo _t('Случайное оружие CS:GO');?></h4>
        
        <?php foreach($data['boxes_random'] as $box):?>
            <div class="text-center item <?php echo $box->checkDrops() ? '' : 'unavailable';?>">
                <a href="<?=url('/box/'.$box->id)?>">
                    <div class="span12 box-image">
                        <?php if($box->getImageUrl()){ ?>
                            <img src="<?= $box->getImageUrl() ?>"  class="img-rounded">                       
                        <?php }else{ ?>
                             <img src="<?= url('assets/img/cases/box3.png') ?>"  class="img-rounded">
                        <?php } ?>
                    </div>
                    <div class="span12 box-info">
                        <p class="box-list-name"><?=$box->name?></p>
                        <?php if($box->checkDrops()):?>
                            <p><i class="fa fa-rub" aria-hidden="true"></i><?=$box->getPrice()?></p>
                        <?php else:?>
                            <p class="box-not-active"><?php echo _t('Временно недоступен');?></p>
                        <?php endif;?>
                    </div>
                </a>
            </div>
        <?php endforeach?>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 section-grey main-items">
        <a name="Кейсы"></a>
        <h4 class="header-blocks-item main-title"><span class="glyphicon glyphicon-folder-close"></span> <?php echo _t('Кейсы CS:GO');?></h4>
        
        <?php foreach($data['boxes'] as $box):?>
            <div class="text-center item <?php echo $box->checkDrops() ? '' : 'unavailable';?>">
                <a href="<?=url('/box/'.$box->id)?>">
                    <div class="span12 box-image">
                        <?php if($box->getImageUrl()){ ?>
                            <img src="<?= $box->getImageUrl() ?>"  class="img-rounded">                       
                        <?php }else{ ?>
                             <img src="<?= url('assets/img/cases/box3.png') ?>"  class="img-rounded">
                        <?php } ?>
                    </div>
                    <div class="span12 box-info">
                        <p class="box-list-name"><?=$box->name?></p>
                        <?php if($box->checkDrops()):?>
                            <p><i class="fa fa-rub" aria-hidden="true"></i><?=$box->getPrice()?></p>
                        <?php else:?>
                            <p class="box-not-active"><?php echo _t('Временно недоступен');?></p>
                        <?php endif;?>
                    </div>
                </a>
            </div>
        <?php endforeach?>
    </div>
        
<? endblock(); ?>

