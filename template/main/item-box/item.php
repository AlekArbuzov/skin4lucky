<h3 class="case-name"><?=$drop->item->name?></h3>
<img class="case-item-image" src="<?=$drop->item->getImageUrl()?>"/><br>
<p><?php echo _t('Поздравляем с выигрышем!')?></p>
<p><?php echo _t('Цена');?> : <?php echo $drop->item->price._t(' руб.');?>
<div class="action-drop">
    <a href="#" drop-id="<?php echo $drop->id?>" action="send" class="btn btn-primary btn-send"><?php echo _t('Получить');?></a>
    <a href="#" drop-id="<?php echo $drop->id?>" action="sell" class="btn btn-primary btn-sell"><?php echo _t('Продать');?></a>
</div>
<a href="#" box-id="<?php echo $drop->box->id?>" class="btn btn-primary btn-buy"><?php echo _t('Повторить');?></a>