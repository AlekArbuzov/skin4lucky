<div class="error">
    <h3><?php echo _t('Ошибка');?></h3>
    <p><?php echo _t('По поводу возврата предмета, свяжитесь, пожалуйста, с администрацией.');?></p>
    <p><?php echo _t('email: ')?> <a href="mailto:<?php echo $admin_email;?>"><?php echo $admin_email;?></a></p>
</div>