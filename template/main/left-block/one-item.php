<div class="one-drop-item" id-drop="<?php echo $drop->id;?>">
    <div class="drop-image">
        <img src="<?php echo $drop->item->getImageUrl();?>">
    </div>
    <div class="drop-item-info">
        <span class="drop-item-title"><?php echo $drop->item->name;?></span>
        <span class="drop-item-user"><?php echo $drop->user->name;?></span>
    </div>
</div>