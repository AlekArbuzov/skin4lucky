<?php include partial('layout') ?>
<?php startblock('content'); ?>
    <div class="col-md-12">
        <ul class="breadcrumb">
            <li><a href="<?=url('/')?>"><i class="fa fa-home" aria-hidden="true"></i> <?php echo _t('Главная');?></a></li>
            <li class="active"><?php echo $page->title;?></li>
        </ul>
        <div class="container content-page">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center"><?php echo $page->title;?></h1>
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo $page->text;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endblock(); ?>