<?php include partial('layout') ?>

<?php startblock('content'); ?>
<div class="col-md-12">
    <ul class="breadcrumb">
        <li><a href="<?=url('/')?>"><i class="fa fa-home" aria-hidden="true"></i> <?php echo _t('Главная');?></a></li>
        <li class="active"><?php echo _t('Партнерство');?></li>
    </ul>
    <div class="table-responsive">
        <table class="table partner-table">
            <thead>
                <tr>
                    <th class="level-column"><?php echo _t('Level')?></th>
                    <th><?php echo _t('To next level')?></th>
                    <th><?php echo _t('Ваш процент')?></th>
                    <th><?php echo _t('Refferral receive')?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($partnerTypes as $partnerType):?>
                <tr>
                    <td>
                        <div class="image-partner">
                            <img src="<?php echo $partnerType->getAvatarUrl();?>">
                        </div>
                    </td>
                    <td>
                        <?php echo $partnerType->sum;?>
                    </td>
                    <td>
                        <?php echo $partnerType->precent;?>%
                    </td>
                    <td>
                        <?php echo $partnerType->receive;?>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-6">
    <h4><b><?php echo _t('Вы популярный блогер? Вебмастер? Стример? Ютубер?');?></b></h4>
    <h4><b><?php echo _t('Возможно представитель киберспортивной команды? Вы в нужном месте!');?></b></h4>
</div>
<? endblock(); ?>