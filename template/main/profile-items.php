<?php foreach($drops as $drop):?>
<div class="text-center image-item box-item-block box-item-profile">
    <?php if($drop->status == \Lucky\Models\Drops::STATUS_OPEN):?>
    <div class="action-btn action-drop">
        <a href="#" drop-id="<?php echo $drop->id?>" action="send" class="btn btn-primary btn-small btn-send page-profile"><?php echo _t('Получить');?></a>
        <a href="#" drop-id="<?php echo $drop->id?>" action="sell" class="btn btn-primary btn-small btn-sell page-profile"><?php echo _t('Продать');?></a>
    </div>
    <?php else:?>
        <?php include partial('main/_drop-status');?>
    <?php endif;?>
    <div class="box-item-image">
        <img src="<?=$drop->item->getImageUrl()?>" />
    </div>
    <div class="name-item">
        <p><?=$drop->item->name?></p>
    </div>
</div>
<?php endforeach ?>
