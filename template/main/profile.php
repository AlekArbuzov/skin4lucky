<?php include partial('layout') ?>

<?php startblock('content'); ?>    
<div class="col-md-12">
    <ul class="breadcrumb">
        <li><a href="<?=url('/')?>"><i class="fa fa-home" aria-hidden="true"></i> <?php echo _t('Главная');?></a></li>
        <li class="active"><?php echo _t('Профиль');?></li>
    </ul>
</div>
<div class="profile_container logged col-md-12">
    <div class="user_info">
        <div class="col-xs-12 col-md-3 ballance text-center">  
            <div class="userpic" style="background:url(<?php echo $user->avatar?>) no-repeat center center; background-size:cover;"></div>
            <div class="steam-link-container">
                <span class="username"><?php echo $user->name?></span><br>
                <a href="http://steamcommunity.com/profiles/<?php echo $user->steam_id?>" target="_blank" class="steam_link"><?php echo _t('профиль Steam');?></a><br>
                <span class="balance"><span class="glyphicon glyphicon-ruble"></span> <?php echo $user->getMoney()?></span><a type="button" data-toggle="modal" data-target="#exampleModal" class="add-funds"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
        </div>
        <div class="col-xs-12 col-md-9">
            <div class="url_form">
                <div><span><i class="glyphicon glyphicon-cog"></i> <?php echo _t('Ваш Referral-URL');?></span></div>
                <input type="text" disabled value="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]".url('/')."?ref=".$user->referralLink ?>" placeholder="<?php echo _t('Введите ссылку на обмен');?>" id="tradeLink" class="<?=$user->tradeLink ? 'success' : '' ?>">
            </div>
            <div class="url_form">
                <div>
                    <span><i class="glyphicon glyphicon-cog"></i> <?php echo _t('Ваш Trade-URL');?></span>
                    <a href="https://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url" target="_blank" class="steam_link">Где взять Trade-URL?</a>
                </div>
                <input type="text" value="<?php echo $user->tradeLink?>" placeholder="<?php echo _t('Введите ссылку на обмен');?>" id="tradeLink" class="<?=$user->tradeLink ? 'success' : '' ?>">
                <button class="btn" id="btnSaveTradeUrl" data-toggle="popover" title="" data-trigger="focus" data-content="">Сохранить</button>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="case_contains">
        <div class="heading"><?php echo _t('Ваши предметы');?>:<span><?php echo $user->dropsCount();?> <?php echo _t('кейсов открыто');?></span></div>
        <?php if(empty($drops)):?>
            <div class="no_items"><?php echo _t('Пользователь еще не открыл ни одного кейса');?></div>
        <?php else:?>
            <div class="col-xs-12 col-sm-12 col-md-12" id="content_results">
                <?php include partial('main/profile-items');?>
            </div>
        <?php endif;?>
        <div class="clearfix"></div>
        <div class="container">
            <?php if($maxPage > 1): ?>
            <div class="text-center load-more">
                <button class="btn-loadMore" id="loadMore" page="2" maxPage="<?php echo $maxPage;?>"><?=_t('Загрузить еще')?></button>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="faq">
        <div class="col-md-6">
        <?php 
        $count = count($articles);
        $delimeter = ceil($count/2);
        $i = 0;
        ?>
        <?php foreach($articles as $article):?>
        <?php if($i >= $delimeter){ $i = 0; ?>
            </div>
            <div class="col-md-6">
            <?php } ?>
            <div class="col-md-12 center-block article-block">
                <div class="article-header">
                    <h4><i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i>  <?php echo $article->title ?></h4>
                </div>
                <div class="">
                    <?php echo $article->text ?>
                </div>
            </div>
            <?php $i++;?>
        <?php endforeach;?>
    </div>
</div>
<? endblock(); ?>