<?php $promocodeForm = !isset($promocodeForm) ? new \Lucky\Forms\Promocode($user) : $promocodeForm; ?>
<form method="post" action="<?php echo $promocodeForm->atts['action'] ?>">
    <div class="form-group">
        <label class="control-label"><?php echo _t('Промо-код')?></label>
        <?php echo $promocodeForm->findElement('code')->render() ?>
    </div>
    <div class="text-right">
        <button type="submit" class="btn btn-success"><?php echo _t('Использовать')?></button>
    </div>
</form>